export default interface Relationship {
    relationship: {
        label: string; 
        category: string;
      }
      object: {
        id: string;
        name: string;
        note: string;
        image: string;
      }
      subject: {
        name:string;
        image:string;
        id:string;
      }
  }
  