export default interface Agent {
    id: string;
    name: string;
    image: string;
    type?: string;
  }
  