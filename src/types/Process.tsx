import Agent from './Agent';
import Commitment from './Commitment';

export default interface Process {
  id: string;
  name: string;
  scope: Agent;
  isFinished: boolean;
  note: string;
  plannedStart: string;
  plannedFinish: string;
  workingAgents: Agent[];
  processPlan: {
    id: string;
    name: string;
  };
  committedInputs: Commitment;
  committedOutputs: Commitment;
  inputs: Commitment;
  outputs: Commitment;
}
