export default interface Quantity {
        numericValue: string;
        unit: {
          id: string;
          name: string;
        }
}