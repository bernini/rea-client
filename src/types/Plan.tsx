import Agent from './Agent';

export default interface Plan {
  id: string;
  name: string;
  due: string;
  note: string;
  plannedOn: string;
  workingAgents: Agent[];
}
