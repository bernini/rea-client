import React from "react";
import styled from "../../themes/styled";
import { compose, withState, withHandlers } from "recompose";
// import { PropsRoute } from "../../helpers/router";
const { getUserQuery } = require("../../graphql/getUser.client.graphql");
// import SmartSentence from './smartsentence';
import { graphql, GraphqlQueryControls, OperationOption } from "react-apollo";
import { LoadingMini, ErrorMini } from "../../components/elements/Loading";
import Event from "../../types/Event";
// import Plans from './plans';
// import Validation from '../Validation';
// import Overview from './overview';
// import Inventory from '../Inventory/inventory';
const GET_FEED = require("../../graphql/queries/getAgentFeed.graphql");
// import { Meh } from '../../components/elements/Icons';
import { Settings, Users } from "../../components/elements/Icons";
import { Grid } from "@zendeskgarden/react-grid";
import Main from "../../components/chrome/Main/Main";
import Breadcrumb from './breadcrumb';
import P from "../../components/typography/P/P";
import H2 from "../../components/typography/H2/H2";
import { clearFix } from "polished";
import media from "styled-media-query";
import CommunityPage from './Community'
const token = localStorage.getItem("oce_token");
import { Route, Switch } from 'react-router-dom';

interface Props {
  providerImage: string;
  handleActivity(): boolean;
  activity: string;
  match: any;
  handleSmartSentence(boolean): boolean;
  closeSmartSentence(): boolean;
  smartSentence: boolean;
  data: any;
  event: string;
  filter: string;
  onFilter(string): string;
  onType(string): string;
  type: string;
  viewer: any;
  loading: any;
  error: any;
  page: number;
  onPage(number): number;
}

type Response = {
  viewer: Data;
};

const withFeed = graphql<{}, Response>(GET_FEED, {
  options: (props: Props) => ({
    variables: {
      id: props.match.params.id,
      token: token,
      month: 2,
      year: 2019
    }
  }),
  props: ({ data, ownProps }) => ({
    ...data,
    ...ownProps
  })
}) as OperationOption<{}, {}>;

interface Data extends GraphqlQueryControls {
  agent: {
    agentEconomicEvents: Event[];
  };
}

const Agent: React.SFC<Props> = props => {
  let agent;
  let user = props.data.user;
  if (props.loading) {
    return <LoadingMini />;
  } else if (props.error) {
    return (
      <ErrorMini
        loading={props.loading}
        refetch={props.data.refetch}
        message={`Error! ${props.error.message}`}
      />
    );
  }
  agent = props.viewer.agent;
  return (
    <Main>
      <Grid>
        <WrapperCont>
          <HeroCont>
            <Breadcrumb name={agent.name} />
            <Hero>
              <Background
                id="header"
                style={{ backgroundImage: `url(${agent.image})` }}
              />
              <HeroActions>
                {/* <Join
                      id={community.localId}
                      followed={community.followed}
                      externalId={community.id}
                    /> */}
                {agent.followed == false ? null : (
                  <EditButton>
                    <Settings
                      width={18}
                      height={18}
                      strokeWidth={2}
                      color={"#fff"}
                    />
                  </EditButton>
                )}
              </HeroActions>
              <HeroInfo>
                <H2>{agent.name}</H2>
                <P>{agent.note}</P>
                <MembersTot>
                  <span>
                    <Users
                      width={18}
                      height={18}
                      strokeWidth={2}
                      color={"#3c3c3c"}
                    />
                  </span>
                  {agent.agentRelationships.slice(0, 3).map((a, i) => {
                    return (
                      <ImgTot
                        key={i}
                        style={{
                          backgroundImage: `url(${a.subject.image ||
                            `https://www.gravatar.com/avatar/${
                              a.subject.id
                            }?f=y&d=identicon`})`
                        }}
                      />
                    );
                  })}{" "}
                  <Tot>
                    {agent.agentRelationships.length - 3 > 0
                      ? `+ ${agent.agentRelationships.length - 3} More`
                      : ``}
                  </Tot>
                </MembersTot>
              </HeroInfo>
            </Hero>
          </HeroCont>

          <Switch>
                {/* <Route
                  path={`/communities/${agent.id}/thread/:id`}
                  component={Thread}
                /> */}
                <Route
                  path={props.match.url}
                  exact
                  render={props => (
                    <CommunityPage
                      {...props}
                      // collections={collections}
                      community={agent}
                      user={user}
                      id={agent.id}
                      type={'community'}
                    />
                  )}
                />
              </Switch>
        </WrapperCont>
      </Grid>
    </Main>
    

              //   <Menu>
              //     <MenuItem
              //       onClick={() => props.onType('overview')}
              //       active={props.type === 'overview' ? true : false}
              //     >
              //       Overview
              //     </MenuItem>
              //     <MenuItem
              //       onClick={() => props.onType('intent')}
              //       active={props.type === 'intent' ? true : false}
              //     >
              //       Plans
              //     </MenuItem>
              //     <MenuItem
              //       onClick={() => props.onType('validation')}
              //       active={props.type === 'validation' ? true : false}
              //     >
              //       Validation
              //     </MenuItem>
              //     <MenuItem
              //       onClick={() => props.onType('wallet')}
              //       active={props.type === 'wallet' ? true : false}
              //     >
              //       Wallet
              //     </MenuItem>
              //     <MenuItem
              //       onClick={() => props.onType('offersneeds')}
              //       active={props.type === 'offersneeds' ? true : false}
              //     >
              //       Offers/Needs
              //     </MenuItem>
              //     <MenuItem
              //       onClick={() => props.onType('inventory')}
              //       active={props.type === 'inventory' ? true : false}
              //     >
              //       Inventory
              //     </MenuItem>
              //     <MenuItem
              //       onClick={() => props.onType('conversations')}
              //       active={props.type === 'conversations' ? true : false}
              //     >
              //       Conversations
              //     </MenuItem>
              //   </Menu>
              // </WrapperHead>

    /* {props.type === 'overview' ? (
                <Overview
                  providerId={user.data.id}
                  events={agent.agentEconomicEvents}
                />
              ) : null}
              {props.type === 'intent' ? (
                <Plans
                  providerId={user.data.id}
                  id={props.match.params.id}
                  providerImage={user.data.image}
                  filter={props.filter}
                  exact
                  page={props.page}
                  onPage={props.onPage}
                  onFilter={props.onFilter}
                />
              ) : null}
              {props.type === 'validation' ? (
                <Validation
                  providerId={user.data.id}
                  id={props.match.params.id}
                  events={agent.agentEconomicEvents}
                />
              ) : null}
              {props.type === 'inventory' ? (
                <Inventory
                  providerId={user.data.id}
                  id={props.match.params.id}
                />
              ) : null}
              {props.type === 'wallet' ? (
                <>
                  <SmartSentence
                    placeholder={'Create a transaction'}
                    providerImage={user.data.image}
                    handleActivity={props.handleActivity}
                    activity={props.activity}
                    scopeId={props.match.params.id}
                    handleSmartSentence={props.handleSmartSentence}
                    isActive={props.smartSentence}
                    closeSmartSentence={props.closeSmartSentence}
                  />
                  <NoItems>
                    <div>
                      <Meh
                        width={32}
                        height={32}
                        color="#ececec"
                        strokeWidth={2}
                      />
                    </div>
                    It seems there isn't very much activity recently...
                  </NoItems>
                </>
              ) : null}
              {props.type === 'offersneeds' ? (
                <>
                  <SmartSentence
                    placeholder={'Create an offer or a need'}
                    providerImage={user.data.image}
                    handleActivity={props.handleActivity}
                    activity={props.activity}
                    scopeId={props.match.params.id}
                    handleSmartSentence={props.handleSmartSentence}
                    isActive={props.smartSentence}
                    closeSmartSentence={props.closeSmartSentence}
                  />
                  <NoItems>
                    <div>
                      <Meh
                        width={32}
                        height={32}
                        color="#ececec"
                        strokeWidth={2}
                      />
                    </div>
                    It seems there isn't very much activity recently...
                  </NoItems>
                </>
              ) : null}
              {props.type === 'conversations' ? (
                <>
                  <SmartSentence
                    placeholder={'Start a new thread'}
                    providerImage={user.data.image}
                    handleActivity={props.handleActivity}
                    activity={props.activity}
                    scopeId={props.match.params.id}
                    handleSmartSentence={props.handleSmartSentence}
                    isActive={props.smartSentence}
                    closeSmartSentence={props.closeSmartSentence}
                  />
                  <NoItems>
                    <div>
                      <Meh
                        width={32}
                        height={32}
                        color="#ececec"
                        strokeWidth={2}
                      />
                    </div>
                    It seems there isn't very much activity recently...
                  </NoItems>
                </>
              ) : null}
            </OverviewCont> */
    /* </Inside> */
    /* {props.smartSentence ? (
              <Overlay onClick={props.handleSmartSentence} />
            ) : null} */
    /* </Content>
      </Wrapper>
    </Body> */
  );
};

// const Actions = styled.div`
//   ${clearFix()};
//   display: flex;
//   border-bottom: 1px solid #edf0f2;
//   & button {
//     border-radius: 4px;
//     background: #f98012;
//     font-size: 13px;
//     font-weight: 600;
//     line-height: 35px;
//     text-align: center;
//     cursor: pointer;
//     color: #f0f0f0;
//     margin: 8px;
//     float: left;
//     padding: 0 16px;
//     display: inline-block;
//   }
//   span {
//     & svg {
//       vertical-align: middle;
//       margin-right: 16px;
//     }
//   }
// `;

// const Header = styled.div`
//   border-bottom: 1px solid #edf0f2;
//   ${clearFix()};
// `;
const HeroActions = styled.div`
  position: absolute;
  top: 20px;
  position: absolute;
  top: 16px;
  left: 16px;
  right: 16px;
`;

const HeroCont = styled.div`
  margin-bottom: 16px;
  border-radius: 6px;
  box-sizing: border-box;
  background: white;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const Tot = styled.div`
  float: left;
  height: 24px;
  line-height: 24px;
  vertical-align: middle;
  margin-left: 8px;
  line-height: 32px;
  height: 32px;
  font-size: 13px;
  color: #cacaca;
  font-weight: 600;
`;

const MembersTot = styled.div`
  margin-top: 0px;
  font-size: 12px;
  cursor: pointer;
  display: inline-block;
  cursor: pointer;
  ${clearFix()} & span {
    margin-right: 8px;
    float: left;
    height: 32px;
    line-height: 32px;
    & svg {
      vertical-align: middle;
    }
  }
`;

const ImgTot = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 50px;
  float: left;
  margin-left: -4px;
  background-size: cover;
  border: 2px solid white;
`;

const EditButton = styled.span`
  display: inline-block;
  width: 40px;
  height: 40px;
  vertical-align: bottom;
  margin-left: 8px;
  border-radius: 40px;
  text-align: center;
  cursor: pointer;
  &:hover {
    background: #f9801240;
  }
  & svg {
    margin-top: 8px;
    text-align: center;
  }
`;

// const Footer = styled.div`
//   height: 30px;
//   line-height: 30px;
//   font-weight: 600;
//   text-align: center;
//   background: #ffefd9;
//   font-size: 13px;
//   border-bottom: 1px solid #e4dcc3;
//   color: #544f46;
// `;

const WrapperCont = styled.div`
  max-width: 1040px;
  margin: 0 auto;
  width: 100%;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
`;

// const CollectionList = styled.div`
//   flex: 1;
//   padding: 8px;
// `;

// const OverviewCollection = styled.div`
//   padding-top: 8px;
//   margin-bottom: -8px;
//   flex: 1;
//   & button {
//     margin-left: 8px
//     margin-bottom: 16px;
//   }
//   & p {
//     margin-top: 0 !important;
//     padding: 8px;
//   }
// `;

const Hero = styled.div`
  width: 100%;
  position: relative;
`;

const Background = styled.div`
  margin-top: 24px;
  height: 250px;
  background-size: cover;
  background-repeat: no-repeat;
  background-color: #e6e6e6;
  position: relative;
  margin: 0 auto;
  background-position: center center;
  ${media.lessThan("medium")`
`} &:before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-image: linear-gradient(to bottom, #002f4b66, #000);
    opacity: 0.9;
    ${media.lessThan("medium")`
    top: 0%;
  `};
  }
`;

const HeroInfo = styled.div`
  padding: 16px;
  & h2 {
    margin: 0;
    font-size: 24px !important;
    line-height: 40px !important;
    margin-bottom: 0px;
    color: ${props => props.theme.styles.colour.base1};
  }
  & p {
    margin-top: 8px;
    color: ${props => props.theme.styles.colour.base2};
  }
  & button {
    span {
      vertical-align: sub;
      display: inline-block;
      height: 30px;
      margin-right: 4px;
    }
  }
`;

// const NoItems = styled.div`
//   text-align: center;
//   font-size: 16px;
//   padding: 8px;
//   color: #838f99;
//   background: white;
//   margin: 10px;
//   padding: 10px;
//   background: white;
//   margin: 10px;
//   border-radius: 6px;
//   border: 1px solid #dddfe2;
//   & div {
//     margin-bottom: 8px;
//   }
// `;

export default compose(
  graphql(getUserQuery),
  withFeed,
  withState("event", "onEvent", "all"),
  withState("activity", "onActivity", null),
  withState("filter", "onFilter", ""),
  withState("type", "onType", "overview"),
  withState("smartSentence", "onSmartSentence", false),
  withState("page", "onPage", 1),
  withHandlers({
    handleEvent: props => val => props.onEvent(val.value),
    closeSmartSentence: props => () => {
      return props.onSmartSentence(false);
    },
    handleSmartSentence: props => val => {
      props.onActivity(null);
      return props.onSmartSentence(val);
    },
    handleActivity: props => value => props.onActivity(value)
  })
)(Agent);

// const CoverInfos = styled.div`
//   padding-left: 16px;
//   padding-top: 16px;
// `;

// const WrapperHead = styled.div`
//   background: #fff;
//   margin: 10px;
//   border-radius: 6px;
//   box-sizing: border-box;
//   border: 5px solid #e2e5ea;
// `;

// const Menu = styled.div`
//   background: #f9f9f9;
//   height: 40px;
//   margin-top: 16px;
//   box-sizing: border-box;
// `;
// const MenuItem = styled.div<{ active?: boolean }>`
//   color: ${props => (props.active ? '#3b99fc' : '#848f99')};
//   cursor: pointer;
//   font-size: 14px;
//   font-weight: 500;
//   letter-spacing: 0.5px;
//   line-height: 40px;
//   display: inline-block;
//   padding: 0 8px;
//   position: relative;
//   text-align: center;
//   margin-right: 16px;
//   &:hover {
//     color: #3b99fc;
//   }
//   &:before {
//     position: absolute;
//     content: '';
//     bottom: -5px;
//     height: 5px;
//     border-radius: 8px;
//     left: 0;
//     width: 100%;
//     background-color: ${props => (props.active ? '#3b99fc' : 'transparent')};
//   }
//   & a {
//     color: #bebebe;
//     text-decoration: none;
//   }
// `;

// const Img = styled.div`
//   width: 60px;
//   height: 60px;
//   display: inline-block;
//   background-size: cover;
//   vertical-align: middle;
//   border-radius: 100%;
//   float: left;
// `;
// const CoverTitle = styled.h3`
//   display: inline-block;
//   margin-left: 10px;
//   font-size: 20px;
//   color: #515d75;
//   margin: 0;
//   margin-left: 16px;
//   height: 60px;
//   line-height: 60px;
// `;
// const Wrapper = styled.div`
//   display: flex;
//   flex-direction: column;
//   box-sizing: border-box;
//   position: relative;
//   flex: 1;
//   overflow-y: overlay;
// `;

// const Content = styled.div`
//   flex: 1 1 auto;
//   will-change: transform;
//   display: flex;
//   flex: 1;
// `;

// const Inside = styled.div`
//   display: flex;
//   flex: 1;
//   flex-direction: column;
//   align-content: center;
//   position: relative;
//   overflow-y: overlay;
//   position: relative;
// `;

// const OverviewCont = styled.div`
//   flex: 1;
//   margin-bottom: 60px;
//   max-width: 1040px;
//   width: 100%;
//   margin: 0 auto;
//   margin-top: 24px;
// `;

// const Body = styled.div`
//   flex: 1;
//   display: flex;
//   flex-direction: row;
//   overflow-y: scroll;
// `;
