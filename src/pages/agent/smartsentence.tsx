import React from 'react';
import styled from 'styled-components';
import { compose, withState, withHandlers } from 'recompose';
import Input from '../../components/inputs/Text/Text';
import Intent from '../../components/createRequirement';

interface Props {
  isActive: boolean;
  handleSmartSentence(boolean): boolean;
  providerImage: string;
  handleNote(): string;
  activity: string;
  handleActivity(string): string;
  note: string;
  placeholder: string;
  scopeId: string;
  closeSmartSentence(): boolean;
}

const SmartText: React.SFC<Props> = props => (
  <SmartSentence isActive={props.isActive}>
    <UserInput onClick={() => props.handleSmartSentence(true)}>
      <Img style={{ backgroundImage: `url(${props.providerImage})` }} />
      {props.isActive ? (
        <SuperInput>
          <Input onChange={props.handleNote} placeholder={props.placeholder} />
        </SuperInput>
      ) : (
        props.placeholder
      )}
    </UserInput>
    {props.isActive ? (
      <Intent
        note={props.note}
        scopeId={props.scopeId}
        closeSmartSentence={props.closeSmartSentence}
      />
    ) : null}
  </SmartSentence>
);

export default compose(
  withState('note', 'onNote', ''),
  withHandlers({
    handleNote: props => event => {
      props.onNote(event.target.value);
    }
  })
)(SmartText);

const Img = styled.div`
  width: 34px;
  height: 34px;
  background-color: ${props => props.theme.styles.colour.base4};
  border-radius: 100px;
  display: inline-block;
  margin-right: 8px;
  margin-left: 16px;
  margin-top: 0px;
  vertical-align: middle;
  background-size: cover;
  margin-top: 20px;
`;

const SuperInput = styled.div`
  flex: 1;
  margin-left: 10px !important;
  & input {
    margin: 0;
    border-width: 0 !important;
    border-color: transparent !important;
    padding: 0 !important;
    background: transparent;
    font-weight: 500;
    font-size: 15px;
    &:active {
      border-width: 0 !important;
      border-color: transparent !important;
    }
    &:hover {
      border-width: 0 !important;
      border-color: transparent !important;
    }
  }
`;

const UserInput = styled.div`
  line-height: 70px;
  flex: 1;
  display: flex;
  height: 70px;
  padding: 0 10px;
  font-weight: 500;
  font-size: 15px;
`;

const SmartSentence = styled.div<{ isActive?: boolean }>`
  height: ${props => (props.isActive ? 'auto' : 'auto')};
  display: flex;
  position: relative;
  z-index: ${props => (props.isActive ? '99999' : 1)};
  flex-direction: column;
  font-size: 14px;
  color: #0000004d;
  font-weight: 500;
  letter-spacing: 0.5px;
  background: #fff;
  cursor: pointer;
  border-radius: 0px;
  box-shadow: 0 1px 1px 0px rgba(0, 0, 0, 0.1);
  z-index: 999;
  margin: 10px;
  border-radius: 3px;
`;
