import React from 'react';
import Feed from '../../components/elements/Event';
import moment from 'moment';
import { clearFix } from 'polished';
import styled from '../../themes/styled';
import Event from '../../types/Event';
import { Meh } from '../../components/elements/Icons';

interface Props {
  providerId: string;
  events: Event[];
}

const Profile: React.SFC<Props> = props => {
  return (
    <Contribution>
      <Events>
        {props.events.length ? (
          props.events.map((ev: Event, i) => (
            <Feed
              scopeId={ev.scope.id}
              commitmentId={
                ev.inputOf ? ev.inputOf.id : ev.outputOf ? ev.outputOf.id : ''
              }
              image={
                ev.provider
                  ? ev.provider.image
                  : `https://www.gravatar.com/avatar/${
                      ev.scope.id
                    }?f=y&d=identicon`
              }
              key={i}
              id={ev.id}
              loggedUserId={props.providerId}
              providerId={ev.provider.id}
              withDelete
              validations={ev.validations}
              primary={
                <FeedItem>
                  <B>{ev.provider ? ev.provider.name : ''}</B>{' '}
                  {ev.action +
                    ' ' +
                    ev.affectedQuantity.numericValue +
                    ' ' +
                    ev.affectedQuantity.unit.name +
                    ' of '}
                  <i>
                    {ev.affects.resourceClassifiedAs
                      ? ev.affects.resourceClassifiedAs.name
                      : ''}
                  </i>
                </FeedItem>
              }
              secondary={ev.note}
              date={moment(ev.start).format('DD MMM')}
            />
          ))
        ) : (
          <NoItems>
            <div>
              <Meh width={32} height={32} color="#ececec" strokeWidth={2} />
            </div>
            It seems there isn't very much activity recently...
          </NoItems>
        )}
      </Events>
    </Contribution>
  );
};

export default Profile;

const NoItems = styled.div`
  text-align: center;
  font-size: 16px;
  color: #838f99;
  & div {
    margin-bottom: 8px;
  }
`;

const Events = styled.div`
  ${clearFix()};
  position: relative;
  margin: 0 -10px;
`;
const Contribution = styled.div`
  padding: 10px;
  background: white;
  margin: 10px;
  border-radius: 6px;
  border: 1px solid #dddfe2;
`;

const FeedItem = styled.div`
  font-size: 14px;
  font-weight: 600;
  color: ${props => props.theme.styles.colour.base2};
  a {
    font-weight: 500;
    color: ${props => props.theme.styles.colour.base2};
  }
`;

const B = styled.b`
  color: ${props => props.theme.styles.colour.base2};
`;
