import * as React from 'react';
import { SFC } from 'react';
import { Trans } from '@lingui/macro';
import { Tabs, TabPanel } from 'react-tabs';
// import Discussion from '../../components/chrome/Discussion/Discussion';
import styled from '../../themes/styled';
import { SuperTab, SuperTabList } from '../../components/elements/SuperTab';
// import TimelineItem from '../../components/elements/TimelineItem';

import Plans from './plans';
import Validation from '../Validation';
import Overview from './overview';
// import Inventory from '../Inventory/inventory';

import { Message, Eye, Globe } from '../../components/elements/Icons';
// import LoadMoreTimeline from '../../components/elements/Loadmore/timeline';
interface Props {
  
  community: any;
  user: any
  type: string;
  id: string
  match: any;
}

const CommunityPage: SFC<Props> = ({
  community,
  match,
  user,
  type,
  id
}) => {
  console.log(match)
  return (
  <WrapperTab>
    <OverlayTab>
      <Tabs>
        <SuperTabList>
          <SuperTab>
            <span>
              <Eye width={20} height={20} strokeWidth={2} color={'#a0a2a5'} />
            </span>
            <h5>
              <Trans>Timeline</Trans>
            </h5>
          </SuperTab>
          <SuperTab>
            <span>
              <Globe
                width={20}
                height={20}
                strokeWidth={2}
                color={'#a0a2a5'}
              />
            </span>
            <h5>
              <Trans>Plans</Trans>
            </h5>
          </SuperTab>
          <SuperTab>
            <span>
              <Globe
                width={20}
                height={20}
                strokeWidth={2}
                color={'#a0a2a5'}
              />
            </span>
            <h5>
              <Trans>Validation</Trans>
            </h5>
          </SuperTab>
          <SuperTab>
            <span>
              <Message
                width={20}
                height={20}
                strokeWidth={2}
                color={'#a0a2a5'}
              />
            </span>{' '}
            <h5>
              <Trans>Discussions</Trans>
            </h5>
          </SuperTab>
        </SuperTabList>
        <TabPanel>
        <Overview
          providerId={user.data.id}
          events={community.agentEconomicEvents}
        />
        </TabPanel>
        <TabPanel>
        <Plans
          providerId={user.data.id}
          id={id}
          providerImage={user.data.image}
          exact
          // page={props.page}
          // onPage={props.onPage}
          // onFilter={props.onFilter}
        />
        </TabPanel>
        <TabPanel>
        <Validation
            providerId={user.data.id}
            id={id}
            events={community.agentEconomicEvents}
          />
        </TabPanel>
        <TabPanel>
          
          </TabPanel>
      </Tabs>
    </OverlayTab>
  </WrapperTab>
)};

export const Footer = styled.div`
  height: 30px;
  line-height: 30px;
  font-weight: 600;
  text-align: center;
  background: #ffefd9;
  font-size: 13px;
  border-bottom: 1px solid #e4dcc3;
  color: #544f46;
`;

export const WrapperTab = styled.div`
  display: flex;
  flex: 1;
  height: 100%;
  border-radius: 6px;
  height: 100%;
  box-sizing: border-box;
  margin-bottom: 16px;
  border-radius: 6px;
  box-sizing: border-box;
  background: white;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;
export const OverlayTab = styled.div`
  height: 100%;
  width: 100%;
  & > div {
    flex: 1;
    height: 100%;
  }
`;

export default CommunityPage;
