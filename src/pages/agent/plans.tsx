import * as React from 'react';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import { LoadingMini, ErrorMini } from '../../components/elements/Loading';
import Plan from '../../types/Plan';
import { Meh } from '../../components/elements/Icons';

import { compose, withState, withHandlers } from 'recompose';
import styled from '../../themes/styled';
const GET_PLANS = require('../../graphql/queries/getPlans.graphql');
import PlanComponent from '../../components/elements/Plan/Plan';
const token = localStorage.getItem('oce_token');
interface Data extends GraphqlQueryControls {
  viewer: {
    agent: {
      id: string;
      agentPlans: Plan[];
    };
  };
}

interface Props {
  providerId: string;
  providerName: string;
  data: Data;
  filter: string;
  id: string;
  providerImage: string;
  toggleValidationModal: any;
  onFilter(string): string;
  page: number;
  onPage(number): number;
  handleOpen(): boolean;
  isOpen: boolean;
}

const withRequirements = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_PLANS, {
  options: (props: Props) => {
    return {
      variables: {
        id: props.id,
        token: token
      }
    };
  }
}) as OperationOption<{}, {}>;

const Requirements: React.SFC<Props> = props => {
  const { loading, error, refetch, viewer } = props.data;
  if (loading) return <LoadingMini />;
  if (error)
    return (
      <ErrorMini
        loading={loading}
        refetch={refetch}
        message={`Error! ${error.message}`}
      />
    );
  let plans = viewer.agent.agentPlans;
  return (
    <Contribution>
      {plans.length ? (
        <EventsInfo>
          {plans.map((plan, i) => (
            <PlanComponent plan={plan} key={i} />
          ))}
        </EventsInfo>
      ) : (
        <NoItems>
          <div>
            <Meh width={32} height={32} color="#ececec" strokeWidth={2} />
          </div>
          It seems there isn't very much activity recently...
        </NoItems>
      )}
    </Contribution>
  );
};

const Contribution = styled.div`
  margin: 10px;
  margin-top: 24px;
`;

const NoItems = styled.div`
  text-align: center;
  font-size: 16px;
  color: #838f99;
  & div {
    margin-bottom: 8px;
  }
`;

const EventsInfo = styled.div`
  margin-bottom: 16px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-column-gap: 8px;
  grid-row-gap: 8px;
`;

export default compose(
  withRequirements,
  withState('isOpen', 'onOpen', false),
  withHandlers({
    handleOpen: props => () => props.onOpen(!props.isOpen)
  })
)(Requirements);
