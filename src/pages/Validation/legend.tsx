import React from 'react';
import styled from '../../themes/styled';
import { clearFix } from 'polished';
export default () => (
  <Legend>
    <h3>Agents</h3>
    <LegendRight>
      <LegendItem>
        <ItemCircle />
        <ItemTitle>non validated</ItemTitle>
      </LegendItem>
      <LegendItem>
        <ItemCircle incompleted />
        <ItemTitle>Incompleted validation</ItemTitle>
      </LegendItem>
      <LegendItem>
        <ItemCircle completed />
        <ItemTitle>Validated</ItemTitle>
      </LegendItem>
    </LegendRight>
  </Legend>
);

const Legend = styled.div`
    ${clearFix()};
    height: 40px;
    padding: 0 8px;
    & h3 {
        font-size: 14px;
        color: #151b26;
        text-transform: uppercase;
        letter-spacing: 0.5px;
        float: left;
        font-weight: 400
        margin: 0;
        line-height: 40px;
    }
`;
const LegendRight = styled.div`
  float: right;
  margin-top: 10px;
`;

const LegendItem = styled.div`
  display: inline-block;
  margin-left: 20px;
`;

const ItemCircle = styled.div<{ completed?: any; incompleted?: any }>`
  width: 16px;
  height: 16px;
  border-radius: 100%;
  background: ${props =>
    props.completed ? '#36b37e' : props.incompleted ? '#c68608' : '#575b6b'};
  margin-right: 8px;
  display: inline-block;
  vertical-align: sub;
`;

const ItemTitle = styled.div`
  text-transform: capitalize;
  font-size: 13px;
  font-weight: 400;
  color: #151b26;
  display: inline-block;
`;
