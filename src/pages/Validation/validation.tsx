import React from 'react';
import AgentTab from './agentTab';
import Legend from './legend';
import styled from '../../themes/styled';
import Event from '../../types/Event';

interface Props {
  id: string;
  events: Event[];
  toggleModal: any;
}

const Validation: React.SFC<Props> = props => {
  let agentsArray: any = [];
  props.events.map(event =>
    agentsArray.push({
      id: event.provider.id,
      name: event.provider.name,
      image: event.provider.image,
      events: []
    })
  );
  function removeDuplicates(myArr, prop) {
    return agentsArray.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  let uniqueAgents = removeDuplicates(agentsArray, 'id');
  uniqueAgents.map((agent, i) =>
    props.events.filter(event => event.provider.id === agent.id).map(item =>
      uniqueAgents[i].events.push({
        id: item.id,
        hours: item.affectedQuantity.numericValue,
        validations: item.validations.length,
        date: item.start
      })
    )
  );
  return (
    <Wrapper style={{ margin: '10px' }}>
      <Legend />
      <ValidationTab>
        {uniqueAgents.map((agent, i) => (
          <AgentTab toggleModal={props.toggleModal} key={i} data={agent} />
        ))}
      </ValidationTab>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 10px 0;
  padding-top: 0;
`;
const ValidationTab = styled.div`
  margin-top: 8px;
`;
export default Validation;
