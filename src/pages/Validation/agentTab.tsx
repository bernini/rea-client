import * as React from 'react';
// import { NavLink } from "react-router-dom";
import { compose, withState, withHandlers } from 'recompose';
import styled from '../../themes/styled';
import { ellipsis, clearFix } from 'polished';
import moment from 'moment';
interface Props {
  data: any;
  toggleModal: any;
}

const AgentLine: React.SFC<Props> = props => {
  let totHours = props.data.events.reduce(function(a, b) {
    return a + b.hours;
  }, 0);
  return (
    <TabAgent>
      <AgentInfo>
        <InfoImage
          style={{
            backgroundImage: `url(${props.data.image ||
              `https://www.gravatar.com/avatar/${
                props.data.id
              }?f=y&d=identicon`})`
          }}
        />
        <AgentResume>
          <InfoName>{props.data.name}</InfoName>
          <h4>
            Tot. {totHours}
            /h
          </h4>
        </AgentResume>
      </AgentInfo>
      <TabContributions>
        {props.data.events.map((event, i) => {
          return (
            <Contribution
              key={i}
              toggleModal={props.toggleModal}
              event={event}
            />
          );
        })}
      </TabContributions>
    </TabAgent>
  );
};

const AgentResume = styled.div`
  display: inline-block;
  ${ellipsis('100px')};
  margin-top: 12px;
  & h4 {
    font-size: 13px;
    letter-spacing: 0.5px;
    margin: 0;
    padding-right: 8px;
    font-weight: 600;
    color: #367cc8;
    text-decoration: underline;
    margin-top: -4px;
    &:last-of-type {
      border-right: 0;
    }
  }
`;

const TabAgent = styled.div`
  min-height: 60px;
  border-bottom: 1px solid #282b2f1a;
  ${clearFix()};
  position: relative;
`;

const AgentInfo = styled.div`
  float: left;
  height: 60px;
  width: 170px;
  position: absolute;
  padding-left: 8px;
`;

const InfoImage = styled.div`
  width: 35px;
  height: 35px;
  border-radius: 3px;
  background: #292d37;
  display: inline-block;
  vertical-align: top;
  margin-right: 8px;
  margin-top: 12px;
  background-size: cover;
`;

const InfoName = styled.div`
  font-size: 14px;
  color: ${props => props.theme.styles.colour.base1};
  font-weight: 400;
  ${ellipsis('100px')};
`;

const TabContributions = styled.div`
  margin-left: 16px;
  margin-left: 170px;
  padding-top: 8px;
`;

const Contribution = compose(
  withState('isVisible', 'toggleVisibility', false),
  withHandlers({
    onHover: props => status => {
      props.toggleVisibility(status);
    }
  })
)(({ toggleModal, event, onHover, isVisible }) => (
  <ContributionWrapper>
    {isVisible ? <Popup event={event} /> : null}
    <ContributionsItem
      incompleted={event.validations === 1 ? true : false}
      completed={event.validations > 1 ? true : false}
      onClick={() => toggleModal(event.id)}
      onMouseEnter={() => onHover(true)}
      onMouseLeave={() => onHover(false)}
    />
  </ContributionWrapper>
));

const Popup = event => {
  return (
    <PopupComp>
      <h2>{moment(event.event.date).format('DD MMM')}</h2>
      <h1>{event.event.hours} hours</h1>
    </PopupComp>
  );
};

const ContributionsItem = styled.div<{ incompleted?: any; completed?: any }>`
  width: 16px;
  height: 16px;
  background: ${props =>
    props.incompleted ? '#c68608' : props.completed ? '#36b37e' : '#cdcdcd'};
  margin-right: 2px;
  display: inline-block;
  vertical-align: sub;
  cursor: pointer;
  margin-bottom: 2px;
`;
const ContributionWrapper = styled.div`
  display: inline-block;
  position: relative;
`;
const PopupComp = styled.div`
  position: absolute;
  top: -50px;
  left: 50%;
  margin-left: -50px;
  width: 100px;
  border-radius: 2px;
  padding: 0 8px;
  background: #1b1c1d90;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  & h1 {
    font-size: 12px;
    font-weight: 300;
    color: #f0f0f0;
    text-align: center;
    line-height: 20px;
    margin: 0;
    padding: 0;
  }
  & h2 {
    font-size: 13px;
    font-weight: 500;
    color: #f0f0f0;
    text-align: center;
    line-height: 20px;
    margin: 0;
    padding: 0;
    margin-top: 4px;
  }
`;

export default AgentLine;
