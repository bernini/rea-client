import React from 'react';
import styled from '../../themes/styled';
import { compose, withState, withHandlers } from 'recompose';
import Home from './Home';
import { PropsRoute } from '../../helpers/router';
import { graphql } from 'react-apollo';
const { getUserQuery } = require('../../graphql/getUser.client.graphql');

export default compose(
  graphql(getUserQuery),
  withState('event', 'onEvent', 'all'),
  withState('page', 'onPage', 1),
  withHandlers({
    handleEvent: props => val => props.onEvent(val.value)
  })
)(props => {
  return (
    <Body>
      <Wrapper>
        <Content>
          <Inside>
            <Overview>
              <PropsRoute
                component={Home}
                path={'/'}
                providerId={props.data.user.data.id}
                providerName={props.data.user.data.name}
                providerImage={props.data.user.data.image}
                event={props.event}
                page={props.page}
                onPage={props.onPage}
              />
            </Overview>
          </Inside>
        </Content>
      </Wrapper>
    </Body>
  );
});

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  position: relative;
  flex: 1;
  overflow-y: overlay;
  min-height: 100vh;
`;

const Content = styled.div`
  flex: 1 1 auto;
  will-change: transform;
  display: flex;
  flex: 1;
`;

const Inside = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-content: center;
  position: relative;
  overflow-y: overlay;
  position: relative;
`;

const Overview = styled.div`
  flex: 1;
  margin-bottom: 60px;
  max-width: 1040px;
  width: 100%;
  margin: 0 auto;
  margin-top: 24px;
`;

const Body = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
`;
