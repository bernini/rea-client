import * as React from 'react';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import { LoadingMini, ErrorMini } from '../../components/elements/Loading';
import Todo from '../Todo/Todo';
import Commitment from '../../types/Commitment';
const GET_COMMITMENTS = require('../../graphql/queries/getCommitments.graphql');
import { compose, withState, withHandlers } from 'recompose';
import Button from '../../components/elements/Button/Button';
import styled from '../../themes/styled';
import OutsideClickHandler from 'react-outside-click-handler';

const token = localStorage.getItem('oce_token');
interface Data extends GraphqlQueryControls {
  viewer: {
    agent: {
      id: string;
      agentCommitments: Commitment[];
      commitmentsMatchingSkills: Commitment[];
    };
  };
}

interface Props {
  providerId: string;
  providerName: string;
  data: Data;
  filter: string;
  id: string;
  providerImage: string;
  toggleValidationModal: any;
  onFilter(string): string;
  page: number;
  onPage(number): number;
  handleOpen(): boolean;
  isOpen: boolean;
}

const withRequirements = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_COMMITMENTS, {
  options: (props: Props) => {
    return {
      variables: {
        id: props.id,
        token: token,
        page: props.page
      }
    };
  }
}) as OperationOption<{}, {}>;

const Requirements: React.SFC<Props> = props => {
  const { loading, error, refetch, viewer } = props.data;
  if (loading) return <LoadingMini />;
  if (error)
    return (
      <ErrorMini
        loading={loading}
        refetch={refetch}
        message={`Error! ${error.message}`}
      />
    );
  let intents = viewer.agent.agentCommitments;
  let filteredIntents: Commitment[] = [];
  if (props.filter === 'active') {
    filteredIntents = intents.filter(i => !i.isFinished);
  } else if (props.filter === 'completed') {
    filteredIntents = intents.filter(i => i.isFinished);
  } else if (props.filter === 'committed') {
    filteredIntents = intents.filter(
      i => (i.provider ? i.provider.id === props.providerId : null)
    );
  } else if (props.filter === 'without process') {
    filteredIntents = intents.filter(i => !i.inputOf && !i.outputOf);
  } else if (props.filter === 'with process') {
    filteredIntents = intents.filter(i => i.inputOf || i.outputOf);
  } else {
    filteredIntents = intents;
  }
  return (
    <EventsInfo>
      <Tagline>
        <ActionsWr>
          <WrapperFilter onClick={props.handleOpen}>
            {props.filter ? props.filter : 'All'}
          </WrapperFilter>
          {props.isOpen ? (
            <OutsideClickHandler onOutsideClick={props.handleOpen}>
              <FilterList>
                <Item onClick={() => props.onFilter('all')}>All</Item>
                <Item onClick={() => props.onFilter('active')}>Active</Item>
                <Item onClick={() => props.onFilter('completed')}>
                  Completed
                </Item>
                <Item onClick={() => props.onFilter('committed')}>
                  Committed
                </Item>
              </FilterList>
            </OutsideClickHandler>
          ) : null}
        </ActionsWr>
      </Tagline>
      <div
        style={{
          padding: ' 8px',
          background: 'white',
          paddingTop: '1px',
          borderRadius: '3px'
        }}
      >
        <Todo
          activeIntents={filteredIntents}
          providerId={props.providerId}
          providerImage={props.providerImage}
          toggleValidationModal={props.toggleValidationModal}
        />
        <Actions>
          <Button
            disabled={props.page === 1}
            onClick={() => props.onPage(props.page - 1)}
          >
            Prev
          </Button>
          <Button
            disabled={intents.length < 25}
            onClick={() => props.onPage(props.page + 1)}
          >
            Next
          </Button>
        </Actions>
      </div>
    </EventsInfo>
  );
};

const ActionsWr = styled.div`
  margin-top: 16px;
`;
const Actions = styled.div`
  margin-top: 16px;
  & button {
    margin-right: 8px;
  }
`;

const Tagline = styled.h3`
  color: #f0f0f0;
  letter-spacing: 1px;
  margin: 0;
  font-size: 14px;
  font-weight: 500;
  position: relative;
  text-transform: capitalize;
  margin-bottom: 18px;
`;

const EventsInfo = styled.div`
  margin-bottom: 16px;
`;

const WrapperFilter = styled.div`
  height: 38px;
  margin-top: 2px;
  margin-right: 0px;
  background: #006cb2;
  border-radius: 2px;
  font-size: 13px;
  padding: 0 10px;
  line-height: 38px;
  width: 80px;
  text-align: center;
  font-weight: 400;
  color: #fafafa;
  cursor: pointer;
`;

const FilterList = styled.div`
  position: absolute;
  left: 0px;
  width: 200px;
  bottom: 0px;
  background: white;
  border-radius: 2px;
  z-index: 99999;
  border: 1px solid #dadada;
`;

const Item = styled.div`
  height: 40px;
  line-height: 40px;
  font-size: 13px;
  padding: 0 10px;
  cursor: pointer;
  color: #333;
  &:hover {
    background: #cecece;
  }
`;

export default compose(
  withRequirements,
  withState('isOpen', 'onOpen', false),
  withHandlers({
    handleOpen: props => () => props.onOpen(!props.isOpen)
  })
)(Requirements);
