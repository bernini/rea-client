import React from "react";
import { compose } from "recompose";
import { withFormik, FormikProps, Field } from "formik";
import * as Yup from "yup";
import AsyncSelect from "react-select/lib/Async";
import { ApolloConsumer } from "react-apollo";
// import withNotif from "../../components/notification";
import { getAllResources } from "../../helpers/asyncQueries";
const addSkill = require("../../graphql/mutations/addSkill.graphql");
const removeSkill = require("../../graphql/mutations/removeSkill.graphql");
import { graphql, OperationOption } from "react-apollo";

const customStyles = {
  control: base => ({
    ...base,
    color: "#333",
    marginBottom: "16px"
  }),
  input: base => ({
    ...base,
    color: "#333"
  }),
  singleValue: base => ({
    ...base,
    color: "#333"
  }),
  placeholder: base => ({
    ...base,
    color: "#333",
    fontSize: "14px"
  })
};

interface FormValues {
  agentSkills: any;
}

interface MyFormProps {
  skills: any[];
  client: any;
  addSkillMutation: any;
  data: any;
  providerId: string;
  removeSkillMutation: any;
}

const withAddSkill = graphql<{}>(addSkill, {
  name: "addSkillMutation"
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);

const withRemoveSkill = graphql<{}>(removeSkill, {
  name: "removeSkillMutation"
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);

interface Props {
  addSkillMutation: any;
  data: any;
  providerId: string;
  removeSkillMutation: any;
}

const SkillSelect = (props: Props & FormikProps<FormValues>) => {
  const {
    setFieldValue,
    values,
    addSkillMutation,
    data,
    providerId,
    removeSkillMutation
  } = props;
  const editSkills = val => {
    let removed = values.agentSkills.filter(
      o => !val.some(o2 => o.value === o2.value)
    );
    let added = val.filter(
      o => !values.agentSkills.some(o2 => o.value === o2.value)
    );
    if (removed.length > 0) {
      let relToDelete = data.viewer.myAgent.agentSkillRelationships.filter(
        r => r.resourceClassification.id === removed[0].value
      );
      removed.map(r => {
        return removeSkillMutation({
          variables: {
            token: localStorage.getItem("oce_token"),
            id: Number(relToDelete[0].id)
          }
        })
          .then(res => {
            return setFieldValue("agentSkills", val);
          })
          .catch(err => console.log(err));
      });
    } else {
      addSkillMutation({
        variables: {
          token: localStorage.getItem("oce_token"),
          skillId: added[0].value,
          agentId: providerId
        },
        update: (store, { data }) => {
          console.log(data);
        }
      })
        .then(res => {
          return setFieldValue("agentSkills", val);
        })
        .catch(err => console.log(err));
    }
  };
  return (
    <ApolloConsumer>
      {client => (
        <Field
          name="agentSkills"
          render={({ field }) => (
            <AsyncSelect
              placeholder="Add more skills..."
              defaultOptions
              cacheOptions
              isClearable={false}
              isMulti
              value={field.value}
              styles={customStyles}
              onChange={val => editSkills(val)}
              loadOptions={val => getAllResources(client, providerId, val)}
            />
          )}
        />
      )}
    </ApolloConsumer>
  );
};

const ModalWithFormik = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => ({
    agentSkills: props.skills
  }),
  validationSchema: Yup.object().shape({
    agentSkills: Yup.object().required("Classification is a required field")
  }),
  handleSubmit: (values, { props }) => val => {}
})(SkillSelect);

export default compose(
  //   withNotif(
  //     "Skills are successfully updated",
  //     "Error, skills have not been updated correctly"
  //   ),
  withAddSkill,
  withRemoveSkill
)(ModalWithFormik);
