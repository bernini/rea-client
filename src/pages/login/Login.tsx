import * as React from 'react';
import { compose } from 'recompose';
import Button from '../../components/elements/Button/Button';
import Text from '../../components/inputs/Text/Text';
import { withFormik, FormikProps, Form, Field } from 'formik';
import * as Yup from 'yup';
import { graphql, OperationOption } from 'react-apollo';
import Alert from '../../components/elements/Alert';
const LoginMutation = require('../../graphql/mutations/login.graphql');
const DeleteNotification = require('../../graphql/mutations/UpdateNotification.graphql');
const UpdateNotification = require('../../graphql/mutations/DeleteNotification.graphql');
import { Redirect, Route, RouteComponentProps } from 'react-router-dom';
import Agent from '../../types/Agent';
const { getUserQuery } = require('../../graphql/getUser.client.graphql');
const { setUserMutation } = require('../../graphql/setUser.client.graphql');
import withNotif from '../../components/NotificationFactory';

import styled from '../../themes/styled';

interface Props {
  errors: any;
  touched: any;
  isSubmitting: boolean;
  login: any;
  history: any;
  data: any;
  onSuccess: any;
  onError: any;
}

interface FormValues {
  username: string;
  password: string;
}

interface MyFormProps {
  login: any;
  history: any;
  data: any;
  onSuccess: any;
  onError: any;
}

/**
 * @param Component
 * @param data {Object} the user object from local cache
 * @param rest
 * @constructor
 */
function RedirectIfAuthenticated({ component: Component, data, ...rest }) {
  console.log(data);
  return (
    <Route
      render={(props: RouteComponentProps & Props) => {
        if (data.user.isAuthenticated) {
          return <Redirect to="/" />;
        }
        return (
          <LoginWithFormik
            data={data}
            login={props.login}
            {...props}
            {...rest}
          />
        );
      }}
    />
  );
}

export interface Args {
  data: {
    isAuthenticated: boolean;
    user: Agent;
  };
}

// get the user auth object from local cache
const withUser = graphql<{}, Args>(getUserQuery);

// get user mutation so we can set the user in the local cache
const withSetLocalUser = graphql<{}, Args>(setUserMutation, {
  name: 'setLocalUser'
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);

const withLogin = graphql<{}>(LoginMutation, {
  name: 'login'
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);
const withUpdateNotification = graphql<{}>(UpdateNotification, {
  name: 'updateNotificationMutation'
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);
const withDeleteNotification = graphql<{}>(DeleteNotification, {
  name: 'deleteNotificationMutation'
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);

const Login = (props: Props & FormikProps<FormValues>) => {
  const { touched, errors } = props;
  return (
    <Wrapper>
      <Title>shroom.</Title>
      <Header>
        <h1>Welcome back</h1>
        <h3 data-testid="desc">
          in your wannabe federated p2p economic network
        </h3>
      </Header>
      <Body>
        <Form>
          <div>
            <Field
              name="username"
              render={({ field }) => (
                <Text
                  value={field.value}
                  name={field.name}
                  onChange={field.onChange}
                  placeholder="Insert your username"
                />
              )}
            />
            {touched.username &&
              errors.username && <Alert>{errors.username}</Alert>}
          </div>
          <div>
            <Field
              name="password"
              render={({ field }) => (
                <Text
                  value={field.value}
                  name={field.name}
                  onChange={field.onChange}
                  type="password"
                  placeholder="Insert your password"
                />
              )}
            />
            {touched.password &&
              errors.password && <Alert>{errors.password}</Alert>}
          </div>
          <Button type="submit" data-testid="login">
            login
          </Button>
        </Form>
      </Body>
    </Wrapper>
  );
};

const LoginWithFormik = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => ({ username: '', password: '' }),
  validationSchema: Yup.object().shape({
    username: Yup.string().required(),
    password: Yup.string()
      .min(4)
      .required()
  }),
  handleSubmit: (values, { props, setSubmitting }) => {
    props
      .login({
        variables: { username: values.username, password: values.password }
      })
      .then(
        data => {
          props.onSuccess();
          localStorage.setItem('oce_token', data.data.createToken.token);
          window.location.reload();
          props.history.push('/');
        },
        e => {
          setSubmitting(false);
          props.onError();
          return null;
        }
      );
  }
})(Login);

export default compose(
  withLogin,
  withUser,
  withNotif('User logged successfully', 'User is not logged'),
  withSetLocalUser,
  withUpdateNotification,
  withDeleteNotification
)(RedirectIfAuthenticated);

const Wrapper = styled.div`
  width: 320px;
  margin: 0 auto;
  margin-top: 20px;
`;

const Title = styled.h3`
  margin-top: 80px;
  font-size: 16px;
  letter-spacing: 3px;
  color: #3b99fc;
`;

const Header = styled.div`
  margin-top: 16px;
  margin-bottom: 40px;
  & h1 {
    font-size: 42px;
    font-weight: 900;
    color: #151b26;
  }
  & h3 {
    margin-top: 16px;
    font-weight: 300;
    color: #151b26;
  }
`;

const Body = styled.div`
  & input {
    margin-bottom: 16px;
  }
`;
