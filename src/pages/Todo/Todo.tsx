import React from "react";
import styled from "styled-components";
import Intent from "../../components/elements/Intent/Intent";
import Commitment from "../../types/Commitment";

interface Props {
  activeIntents: Commitment[];
  providerId: string;
  providerImage: string;
  toggleValidationModal(): boolean;
}

const Todo = (props: Props) => {
  return (
    <WrapperIntents>
      {props.activeIntents.map((intent, i) => (
        <Intent
          key={i}
          toggleValidationModal={props.toggleValidationModal}
          data={intent}
          scopeId={intent.scope ? intent.scope.id : null}
          providerId={props.providerId}
          providerImage={props.providerImage}
        />
      ))}
    </WrapperIntents>
  );
};

export default Todo;

const WrapperIntents = styled.div`
  position: relative;
`;
