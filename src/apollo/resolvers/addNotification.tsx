import gql from "graphql-tag";

let nextNotifId = 0;

export default function addNotification(_, { message, type }, { cache }) {
    let newId = nextNotifId++;
    const query = gql`
      query GetNotifications {
        notifications @client {
          __typename
          id
          message
          type
        }
      }
    `;
    const previousState = cache.readQuery({ query });
    const newNotif = {
      id: newId,
      message,
      type,
      __typename: "Notification"
    };
    const data = {
      notifications: previousState.notifications.concat([newNotif])
    };
    cache.writeQuery({ query, data: data });
    return newNotif;
}
