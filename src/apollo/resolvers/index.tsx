import setUser from './setUser';
import addNotification from './addNotification';
import deleteNotification from './deleteNotification';

export default {
  Mutation: {
    setUser,
    addNotification,
    deleteNotification
  }
};
