import gql from "graphql-tag";

export default function addNotification(_, { id }, { cache }) {
    const query = gql`
    query GetNotifications {
      notifications @client {
        __typename
        id
        message
        type
      }
    }
  `;
  const previousState = cache.readQuery({ query });
  const data = {
    notifications: previousState.notifications.filter(
      item => item.id !== id
    )
  };
  cache.writeQuery({ query, data });
  return data;
}
