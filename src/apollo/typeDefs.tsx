export default `
  type User {
    id: Int!
    name: String!
    email: String!
    note: String!
    type: String
    image: String
  }
  
  type Notification {
    id: Int!
    when: String!
    type: String!
    content: String!
  }
`;
