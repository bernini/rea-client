```js
const { Label } = require('@zendeskgarden/react-checkboxes');

<div>
  <Checkbox>
    <Label>unchecked</Label>
  </Checkbox>
  <Checkbox checked>
    <Label>checked</Label>
  </Checkbox>
</div>;
```
