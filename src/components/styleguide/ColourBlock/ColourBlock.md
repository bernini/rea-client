### Base colours:

```js
<ColourBlock colour="base1">base1</ColourBlock>
<ColourBlock colour="base2">base2</ColourBlock>
<ColourBlock colour="base3">base3</ColourBlock>
<ColourBlock colour="base4">base4</ColourBlock>
<ColourBlock colour="base5">base5</ColourBlock>
<ColourBlock colour="base6">base6</ColourBlock>
```

### Primary colour:

```js
<ColourBlock colour="primary">primary</ColourBlock>
```

### Secondary colours:

```js
<ColourBlock colour="collection">collection</ColourBlock>
<ColourBlock colour="community">community</ColourBlock>
```
