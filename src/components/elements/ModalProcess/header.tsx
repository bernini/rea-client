import React from 'react';
import styled from 'styled-components';
import { Clock } from '../Icons';
import Agents from './agents';
import Agent from '../../../types/Agent';
import { clearFix } from 'polished';
interface Props {
  from: string;
  to: string;
  scope: {
    id: string;
    image: string;
    name: string;
  };
  plan: {
    name: string;
  };
  title: string;
  note: string;
  agents: Agent[];
}

const Header: React.SFC<Props> = props => (
  <Wrapper>
    <WrapperAction>
      <Date>
        <span>
          <Clock strokeWidth={2} width={16} height={16} color="#17394d" />
        </span>{' '}
        From {props.from} to {props.to}
      </Date>
      <Agents agents={props.agents} />
    </WrapperAction>
    <WrapperInfo>
      <Title>{props.title}</Title>
      {props.note ? <Note>{props.note}</Note> : null}
    </WrapperInfo>
  </Wrapper>
);

export default Header;

const Wrapper = styled.div`
  padding: 16px;
`;

const WrapperInfo = styled.div``;
const Title = styled.h1`
  font-size: 22px;
  color: #17394d;
  letter-spacing: 0;
  margin-bottom: 0px;
  line-height: 32px;
  font-weight: 600;
  margin: 0 !important;
`;
const Note = styled.div`
  font-size: 15px;
  line-height: 24px;
  letter-spacing: 0;
  font-weight: 300;
  margin: 0;
  margin-top: 8px;
  color: #17394d;
  margin-bottom: 16px;
`;
const WrapperAction = styled.div`
  ${clearFix()};
`;
const Date = styled.div`
  border-radius: 3px;
  height: 26px;
  line-height: 26px;
  color: #17394d;
  display: inline-block;
  font-size: 13px;
  font-weight: 500;
  & span {
    margin-right: 4px;
    vertical-align: sub;
    display: inline-block;
  }
`;
