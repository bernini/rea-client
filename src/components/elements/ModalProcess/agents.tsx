import React from 'react';
import styled from 'styled-components';
import { Users } from '../Icons';
import Agent from '../../../types/Agent';
interface Props {
  agents: Agent[];
}

const Agents: React.SFC<Props> = props => (
  <Wrapper>
    <Title>
      <span>
        <Users strokeWidth={2} width={20} height={20} color="#666" />
      </span>
    </Title>
    <Content>
      {props.agents.map((a, i) => (
        <React.Fragment key={i}>
          <Img key={i} style={{ backgroundImage: `url(${a.image})` }} />
        </React.Fragment>
      ))}
    </Content>
  </Wrapper>
);

export default Agents;
const Wrapper = styled.div`
  float: right;
`;
const Content = styled.div`
  display: inline-block;
`;
const Title = styled.div`
  font-size: 14px;
  letter-spacing: 0.5px;
  display: inline-block;
  vertical-align: text-bottom;
  & span {
    margin-right: 8px;
    display: inline-block;
  }
`;
const Img = styled.div`
  display: inline-block;
  height: 24px;
  width: 24px;
  border-radius: 40px;
  background-size: cover;
  margin-right: 4px;
`;
