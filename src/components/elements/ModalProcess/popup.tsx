import React from 'react';
import styled from 'styled-components';
import Commitment from '../../../types/Commitment';
import { Form, Field, FormikProps, withFormik } from 'formik';
import Text from '../../inputs/Text/Text';
import Textarea from '../../inputs/TextArea/Textarea';
import * as Yup from 'yup';
import { clearFix } from 'polished';
import Button from '../Button/Button';
import moment from 'moment';
import { compose } from 'recompose';
import withNotif from '../../NotificationFactory';
const CREATE_EVENT = require('../../../graphql/mutations/createOutput.graphql');
// const event = require("../../../graphql/fragments/economicEvent.graphql");
import { graphql, OperationOption } from 'react-apollo';

const withCreateEvent = graphql<{}>(CREATE_EVENT, {
  name: 'createEventMutation'
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);

interface Props {
  output: Commitment;
  providerId: string;
  providerImage: string;
  onError: any;
  onSuccess: any;
  processId: number;
  onFeedOpen(boolean): boolean;
  createEventMutation: any;
}

interface FormValues {
  total: number;
  trackingIdentifier: string;
  note: string;
}
const Todo = (props: Props & FormikProps<FormValues>) => {
  return (
    <Popup>
      <Form>
        <Row>
          <label>
            How many {props.output.resourceClassifiedAs.name} do you want to{' '}
            {props.output.action}?
          </label>
          <ContainerForm>
            <Field
              name="total"
              render={({ field }) => (
                <Text
                  type="number"
                  value={field.value}
                  name={field.name}
                  onChange={field.onChange}
                  placeholder="Total..."
                />
              )}
            />
          </ContainerForm>
        </Row>
        <Row>
          <label>Do you want to assign a specific name at the resource?</label>
          <ContainerForm>
            <Field
              name="trackingIdentifier"
              render={({ field }) => (
                <Text
                  value={field.value}
                  name={field.name}
                  onChange={field.onChange}
                  placeholder="Name..."
                />
              )}
            />
          </ContainerForm>
        </Row>
        <Row big>
          <label>Do you want to add a note?</label>
          <ContainerForm>
            <Field
              name="note"
              render={({ field }) => (
                <Textarea
                  placeholder="What the collection is about..."
                  name={field.name}
                  value={field.value}
                  onChange={field.onChange}
                />
              )}
            />
          </ContainerForm>
        </Row>
        <Actions>
          <Button
            disabled={props.isSubmitting}
            type="submit"
            style={{ marginLeft: '10px' }}
          >
            Create
          </Button>
          <Button secondary onClick={() => props.onFeedOpen(false)}>
            Cancel
          </Button>
        </Actions>
      </Form>
    </Popup>
  );
};

const CompWithFormik = withFormik<Props, FormValues>({
  mapPropsToValues: props => ({
    total: 0,
    note: '',
    trackingIdentifier: ''
  }),
  validationSchema: Yup.object().shape({
    total: Yup.number(),
    note: Yup.string(),
    trackingIdentifier: Yup.string()
  }),
  handleSubmit: (values, { props }) => {
    let date = moment().format('YYYY-MM-DD');
    let eventMutationVariables = {
      token: localStorage.getItem('oce_token'),
      id: props.providerId,
      providerId: props.providerId,
      receiverId: props.output.scope.id,
      outputOfId: props.processId,
      trackingIdentifier: values.trackingIdentifier,
      commitmentId: props.output.id,
      action: props.output.action,
      scopeId: props.output.scope.id,
      createResource: true,
      resourceNote: values.note,
      affectedNumericValue: String(values.total),
      affectedUnitId: props.output.committedQuantity.unit.id,
      start: date,
      affectedResourceClassifiedAsId: props.output.resourceClassifiedAs.id
    };
    return props
      .createEventMutation({
        variables: eventMutationVariables
        // update: (store, { data }) => {
        //     const fragment = gql`
        //   fragment Comm on Commitment {
        //     id
        //     fulfilledBy {
        //       fulfilledBy {
        //         ...BasicEvent
        //       }
        //     }
        //   }
        //   ${event}
        // `
        //   const commitment = store.readFragment({
        //     id: `Commitment-${
        //       props.commitmentId
        //     }`,
        //     fragment: fragment,
        //     fragmentName: "Comm",
        //   });
        //   const ev = {
        //     __typename: "EconomicEvent",
        //     action: data.createEconomicEvent.economicEvent.action,
        //     requestDistribution: data.createEconomicEvent.economicEvent.requestDistribution,
        //     start: data.createEconomicEvent.economicEvent.start,
        //     id: data.createEconomicEvent.economicEvent.id,
        //     scope: data.createEconomicEvent.economicEvent.scope,
        //     note: data.createEconomicEvent.economicEvent.note,
        //     provider: data.createEconomicEvent.economicEvent.provider,
        //     inputOf: data.createEconomicEvent.economicEvent.inputOf,
        //     isValidated: false,
        //     fulfills: data.createEconomicEvent.economicEvent.fulfills,
        //     validations: [],
        //     affects: data.createEconomicEvent.economicEvent.affects,
        //     affectedQuantity: data.createEconomicEvent.economicEvent.affectedQuantity
        //   };

        //   commitment.fulfilledBy.unshift({
        //     fulfilledBy: ev,
        //     __typename: "Fulfillment"
        //   });
        //   store.writeFragment({
        //     id: `Commitment-${
        //       props.commitmentId
        //     }`,
        //     fragment: fragment,
        //     fragmentName: "Comm",
        //     data: commitment,
        //   });
        //   }
      })
      .then(res => props.onSuccess())
      .catch(err => props.onError());
  }
})(Todo);

export default compose(
  withNotif('Ouput created', 'The output is not created successfully'),
  withCreateEvent
)(CompWithFormik);

const Actions = styled.div`
  ${clearFix()};
  height: 60px;
  padding-top: 10px;
  padding-right: 10px;
  & button {
    float: right;
  }
`;

const Popup = styled.div`
  margin-top: -20px;
`;

const Row = styled.div<{ big?: boolean }>`
  ${clearFix()};
  border-bottom: 1px solid rgba(151, 151, 151, 0.2);
  height: ${props => (props.big ? '160px' : '120px')};
  padding: 16px;
  & label {
    width: 200px;
    line-height: 40px;
    color: #333;
  }
`;

const ContainerForm = styled.div``;
