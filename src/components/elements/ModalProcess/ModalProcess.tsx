import * as React from 'react';
import Modal from '../Modal';
import styled from '../../../themes/styled';
import moment from 'moment';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import { LoadingMini, ErrorMini } from '../../elements/Loading';
import Process from '../../../types/Process';
import { compose } from 'recompose';
const GET_PROCESS = require('../../../graphql/queries/getProcess.graphql');
import Header from './header';
const { getUserQuery } = require('../../../graphql/getUser.client.graphql');
import Todo from '../../../pages/Todo/Todo';
import { Meh } from '../../elements/Icons';
import Outputs from './outputs';

interface Props {
  toggleModal(string): any;
  closeModal(strin): boolean;
  modalIsOpen?: boolean;
  data: any;
  match: any;
  planId: string;
  viewer: Data;
  loading: any;
  error: any;
  refetch: any;
}

const token = localStorage.getItem('oce_token');
interface Data extends GraphqlQueryControls {
  process: Process;
}

const withProcess = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_PROCESS, {
  options: (props: Props) => {
    return {
      variables: {
        id: props.match.params.id,
        token: token
      }
    };
  },
  props: ({ data, ownProps }) => ({
    ...data,
    ...ownProps
  })
}) as OperationOption<{}, {}>;

const CreateCommunityModal = (props: Props) => {
  const { closeModal, modalIsOpen } = props;
  const { loading, error, refetch, viewer } = props;
  let container: any = viewer;
  return (
    <Modal
      isOpen={modalIsOpen}
      toggleModal={() => closeModal(`/plan/${props.planId}`)}
    >
      {loading ? (
        <LoadingMini />
      ) : error ? (
        <ErrorMini
          loading={loading}
          refetch={refetch}
          message={`Error! ${error.message}`}
        />
      ) : (
        <Container>
          <Header
            title={viewer.process.name}
            note={viewer.process.note}
            from={moment(viewer.process.plannedStart).format('DD MMM')}
            to={moment(viewer.process.plannedFinish).format('DD MMM')}
            scope={viewer.process.scope}
            plan={viewer.process.processPlan}
            agents={viewer.process.workingAgents}
          />
          <Wrapper>
            <h4>Result</h4>
            {container.process.committedOutputs.length ? (
              <Outputs
                processId={container.process.id}
                activeIntents={container.process.committedOutputs}
                providerId={props.data.user.data.id}
                providerImage={props.data.user.data.image}
                toggleValidationModal={() => true}
              />
            ) : (
              <NoItems>
                <div>
                  <Meh width={32} height={32} color="#ececec" strokeWidth={2} />
                </div>
                This process does not produce any outputs
              </NoItems>
            )}
            <h4>Inputs</h4>
            <Bar
              percentage={
                (container.process.committedInputs.filter(i => i.isFinished)
                  .length *
                  100) /
                container.process.committedInputs.length
              }
            />
            {container.process.committedInputs.length ? (
              <Todo
                activeIntents={container.process.committedInputs}
                providerId={props.data.user.data.id}
                providerImage={props.data.user.data.image}
                toggleValidationModal={() => true}
              />
            ) : (
              <NoItems>
                <div>
                  <Meh width={32} height={32} color="#ececec" strokeWidth={2} />
                </div>
                This process does not have any inputs
              </NoItems>
            )}
          </Wrapper>
        </Container>
      )}
    </Modal>
  );
};

export default compose(
  graphql(getUserQuery),
  withProcess
)(CreateCommunityModal);

const Bar = styled.div<{ percentage?: any }>`
  height: 4px;
  border-radius: 14px;
  background: #dee6f2;
  margin-bottom: 10px;
  position: relative;
  border: 8px solid rgba(255, 255, 255, 0);
  margin-top: 8px;
  &:before {
    content: '';
    position: absolute;
    width: ${props => props.percentage}%;
    height: 6px;
    background-color: #62cfa9;
    border-radius: 4px;
    display: block;
    left: 0;
    top: -3px;
  }
`;

const Container = styled.div`
  background-color: #ebeef0;
  border-radius: 6px;
`;

const Wrapper = styled.div`
  padding: 16px;
  padding-top: 0;
  h4 {
    font-size: 14px;
    color: #17394d;
    margin: 0;
    margin-top: 8px;
  }
`;

const NoItems = styled.div`
  text-align: center;
  font-size: 16px;
  color: #838f99;
  background: white;
  padding: 10px 0;
  border: 1px solid #97979740;
  margin-top: 8px;
  border-radius: 6px;
  background: #fff;

  & div {
    margin-bottom: 8px;
  }
`;
