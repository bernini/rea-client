import * as React from "react";
import Modal from "../Modal";
import styled from "../../../themes/styled";
import { clearFix } from "polished";
import H5 from "../../typography/H5/H5";
import Textarea from "../../inputs/TextArea/Textarea";
import { Check } from "../Icons";
import { Query } from "react-apollo";
import { withFormik, FormikProps, Field } from "formik";
import * as Yup from "yup";
import moment from "moment";
import CreateValidation, { DeleteValidation } from "../ToggleValidation";
import { LoadingMini, ErrorMini } from "../Loading";
const GET_EVENT = require("../../../graphql/queries/getEvent.graphql");

interface Props {
  toggleModal(string): any;
  closeModal(): boolean;
  modalIsOpen?: boolean;
  contributionId: string;
  errors: any;
  touched: any;
  myId: string;
  isSubmitting: boolean;
}

interface FormValues {
  note: string;
}

interface MyFormProps {
  contributionId: string;
  closeModal(): boolean;
  toggleModal: any;
  myId: string;
  modalIsOpen?: boolean;
}

const CreateCommunityModal = (props: Props & FormikProps<FormValues>) => {
  const { closeModal, modalIsOpen, contributionId, myId, values } = props;
  return (
    <Modal isOpen={modalIsOpen} toggleModal={closeModal}>
      <Container>
        <Header>
          <H5>Validate an event</H5>
        </Header>
        <Query
          query={GET_EVENT}
          variables={{
            token: localStorage.getItem("oce_token"),
            id: Number(contributionId)
          }}
        >
          {({ loading, error, data, refetch }) => {
            if (loading) return <LoadingMini />;
            if (error)
              return (
                <ErrorMini
                  loading={loading}
                  refetch={refetch}
                  message={`Error! ${error.message}`}
                />
              );
            return (
              <Wrapper>
                <Sentence>
                  <Photo
                    style={{
                      backgroundImage: `url(${
                        data.viewer.economicEvent.provider.image
                      })`
                    }}
                  />
                  <SentenceInfo>
                    <SentenceText>
                      {data.viewer.economicEvent.provider.name}{" "}
                      {data.viewer.economicEvent.action}{" "}
                      <b>
                        {data.viewer.economicEvent.affectedQuantity
                          .numericValue +
                          " " +
                          data.viewer.economicEvent.affectedQuantity.unit.name}
                      </b>
                    </SentenceText>
                    <Secondary>
                      <Date>
                        {moment(data.viewer.economicEvent.start).format(
                          "DD MMM"
                        )}
                      </Date>
                    </Secondary>
                  </SentenceInfo>
                  <Note>{data.viewer.economicEvent.note}</Note>
                </Sentence>
                <Validations>
                  {data.viewer.economicEvent.validations.map((val, i) => {
                    return (
                      <ValidationsItem key={i}>
                        <ValMain>
                          <span style={{ float: "left" }}>
                            <Check
                              width={18}
                              height={18}
                              strokeWidth={1}
                              color="#36B37E"
                            />
                          </span>
                          <ValMainName>
                            {val.validatedBy.name} validated
                          </ValMainName>
                        </ValMain>
                        <ValMainNote>{val.note}</ValMainNote>
                        {Number(val.validatedBy.id) === Number(myId) ? (
                          <DeleteValidation
                            validationId={val.id}
                            eventId={data.viewer.economicEvent.id}
                          />
                        ) : null}
                      </ValidationsItem>
                    );
                  })}
                </Validations>
                {data.viewer.economicEvent.validations.findIndex(
                  item => Number(item.validatedBy.id) === Number(myId)
                ) !== -1 ? null : data.viewer.economicEvent.validations
                  .length >= 2 ? null : data.viewer.economicEvent.provider
                  .id === myId ? null : (
                  <div style={{ marginTop: "24px" }}>
                    <Field
                      name="note"
                      render={({ field /* _form */ }) => (
                        <Textarea
                          {...field}
                          placeholder="Type the validation note..."
                        />
                      )}
                    />
                    <Footer>
                      <CreateValidation
                        eventId={contributionId}
                        providerId={myId}
                        note={values.note}
                      />
                    </Footer>
                  </div>
                )}
              </Wrapper>
            );
          }}
        </Query>
      </Container>
    </Modal>
  );
};

const ModalWithFormik = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => ({
    note: ""
  }),
  validationSchema: Yup.object().shape({
    note: Yup.string()
  }),
  handleSubmit: (values, { props }) => {}
})(CreateCommunityModal);

export default ModalWithFormik;

const Container = styled.div`
  font-family: ${props => props.theme.styles.fontFamily};
`;

const Wrapper = styled.div`
  padding: 8px;
`;

const Header = styled.div`
  height: 60px;
  border-bottom: 1px solid rgba(151, 151, 151, 0.2);
  & h5 {
    text-align: center !important;
    line-height: 60px !important;
    margin: 0 !important;
  }
`;

const Sentence = styled.div`
  ${clearFix()};
`;

const Photo = styled.div`
  float: left;
  width: 35px;
  height: 35px;
  border-radius: 3px;
  background: ${props => props.theme.styles.colour.base1};
  background-size: cover;
`;

const SentenceText = styled.div`
  font-size: 14px;
  font-weight: 600;
  color: ${props => props.theme.styles.colour.base2};
  letter-spacing: 0.5px;
  a {
    font-weight: 500;
    color: ${props => props.theme.styles.colour.base2};
  }
`;
const SentenceInfo = styled.div`
  margin-left: 44px;
`;
const Secondary = styled.div`
  ${clearFix()};
`;

const Date = styled.div`
  float: left;
  font-size: 13px;
  color: #73808f;
  line-height: 21px;
  font-weight: 500;
`;

const Note = styled.div`
  color: ${props => props.theme.styles.colour.base2};
  margin-top: 8px;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: 0.5px;
`;

const Validations = styled.div`
  margin: -8px;
  margin-top: 8px;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  background: #ebebeb;
`;

const ValidationsItem = styled.div`
  border-top: 1px solid #dedede;
  padding: 8px;
  & button {
    margin-top: 16px;
  }
`;

const ValMain = styled.div`
  ${clearFix()};
`;
const ValMainName = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: ${props => props.theme.styles.colour.base1};
  margin-left: 8px;
  letter-spacing: 0.5px;
  float: left;
`;

const ValMainNote = styled.div`
  font-weight: 400;
  font-size: 14px;
  margin-top: 4px;
  color: ${props => props.theme.styles.colour.base3};
  max-width: 480px;
`;

const Footer = styled.div`
  background: #e5e5e5;
  margin: 8px -8px -16px;
  ${clearFix()};
`;
