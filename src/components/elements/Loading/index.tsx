import * as  React from 'react'
import {Loading, Cross} from '../../elements/Icons'
import Button from '../Button/Button';
import styled from '../../../themes/styled';


const Wrapper = styled.div`
    margin-top: 20px;
    text-align: center;
    & h1 {
        display: inline-block;
        margin-left: 10px;
        color: #606984;
        font-size: 16px;
        vertical-align: middle;
    }
`
const LoaderIcon = styled.span`
    vertical-align: sub;
`

const WrapperError = styled.div`
font-size: 26px;
color: #f0f0f0a3;
line-height: 30px;
font-weight: 500;
`


export const LoadingMini: React.SFC<{}> = () => (
    <Wrapper>
      <div>
        <LoaderIcon><Loading width={24} height={24} strokeWidth={1} color='#606984' /></LoaderIcon>
        <h1>Loading...</h1>
      </div>
    </Wrapper>
)

interface ErrorProps {
  message: string,
  loading: boolean,
  refetch: any
}

export const ErrorMini: React.SFC<ErrorProps> = ({message, loading, refetch}) => (
    <WrapperError>
        <div><Cross width={26} height={26} strokeWidth={1} color='#f0f0f0a3' /></div>
        <h1>{message}</h1>
        {loading ? <Button disabled>Wait...</Button> : <Button onClick={() => refetch()}>Refresh</Button>}
    </WrapperError>
)

