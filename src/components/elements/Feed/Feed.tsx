import React from 'react';
import styled from '../../../themes/styled';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import Event from '../../../types/Event';
import EventItem from '../../elements/Event/index';
import { compose } from 'recompose';
import moment from 'moment';
import { LoadingMini, ErrorMini } from '../../elements/Loading';

const GET_EVENTS = require('../../../graphql/queries/getEconomicEvents.graphql');

const token = localStorage.getItem('oce_token');

interface Props {
  data: Data;
}

interface Data extends GraphqlQueryControls {
  viewer: {
    filteredEconomicEvents: Event[];
  };
}

const withEvents = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_EVENTS, {
  options: (props: Props) => ({
    variables: {
      token: token
    }
  })
}) as OperationOption<{}, {}>;

const FeedComponent: React.SFC<Props> = props => {
  if (props.data.loading) return <LoadingMini />;
  if (props.data.error)
    return (
      <ErrorMini
        loading={props.data.loading}
        refetch={props.data.refetch}
        message={`Error! ${props.data.error.message}`}
      />
    );
  let events = props.data.viewer.filteredEconomicEvents;
  return (
    <Feed>
      <Title>Recent activities</Title>
      {events.map((ev, i) => (
        <EventItem
          commitmentId={''}
          scopeId={ev.scope ? ev.scope.id : 'null'}
          image={
            ev.provider.image ||
            `https://www.gravatar.com/avatar/${ev.provider.id}?f=y&d=identicon`
          }
          key={i}
          id={ev.id}
          loggedUserId={''}
          light={true}
          providerId={ev.provider.id}
          validations={ev.validations}
          primary={
            <FeedItem>
              <B>{ev.provider ? ev.provider.name : ''}</B>{' '}
              {ev.action +
                ' ' +
                ev.affectedQuantity.numericValue +
                ' ' +
                ev.affectedQuantity.unit.name +
                ' of '}
              <i>{ev.affects.resourceClassifiedAs.name}</i>
            </FeedItem>
          }
          secondary={ev.note}
          date={moment(ev.start).format('DD MMM')}
        />
      ))}
    </Feed>
  );
};

export default compose(withEvents)(FeedComponent);

const FeedItem = styled.div`
  font-size: ${props => props.theme.styles.fontSize.sm};
  color: #bebebe;
`;

const B = styled.b`
  font-weight: 500;
  color: #fff;
`;

const Feed = styled.div`
  width: 300px;
  background: #1f2227;
  margin-left: 16px;
  padding: 8px;
  padding-top: 16px;
`;

const Title = styled.h3`
  font-size: 16px;
  margin: 0;
  color: rgba(250, 250, 250, 1);
  border-bottom: 1px solid rgba(250, 250, 250, 0.2);
  letter-spacing: 1px;
  border-bottom: 1px solid rgba(250, 250, 250, 0.2);
  padding-bottom: 8px;
`;
