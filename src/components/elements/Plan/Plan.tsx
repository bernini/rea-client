import * as React from 'react';
import styled from '../../../themes/styled';
import Plan from '../../../types/Plan';
import { clearFix } from 'polished';
import moment from 'moment';
import { NavLink } from 'react-router-dom';
import { Clock } from '../Icons';
interface Props {
  plan: Plan;
}

const PlanComponent: React.SFC<Props> = props => (
  <A to={`/plan/${props.plan.id}`}>
    <Wrapper>
      <Due>
        <span>
          <Clock width={16} height={16} strokeWidth={2} color={'#fff'} />
        </span>
        {moment(props.plan.due).format('DD MMM Y')}
      </Due>
      <Title>{props.plan.name}</Title>
      <Note>{props.plan.note}</Note>
      <Actions>
        <Right>
          <Members>
            {props.plan.workingAgents.slice(0, 3).map((a, i) => {
              return (
                <Img
                  key={i}
                  style={{
                    backgroundImage: `url(${a.image ||
                      `https://www.gravatar.com/avatar/${
                        a.id
                      }?f=y&d=identicon`})`
                  }}
                />
              );
            })}{' '}
            <Tot>
              {props.plan.workingAgents.length - 3 > 0
                ? `+ ${props.plan.workingAgents.length - 3} More`
                : ''}
            </Tot>
          </Members>
        </Right>
      </Actions>
    </Wrapper>
  </A>
);

const A = styled(NavLink)`
  text-decoration: none;
  & * {
    text-decoration: none;
  }
`;

const Wrapper = styled.div`
  border-radius: 6px;
  padding: 8px;
  padding-bottom: 0;
  min-height: 160px;
  max-height: 160px;
  position: relative;
  cursor: pointer;
  background: #3b99fc;
  &:hover {
    background: #3586dc;
  }
`;

const Due = styled.div`
  font-size: 13px;
  color: #ffffffdb;
  font-weight: 600;
  & span {
    vertical-align: sub;
    margin-right: 8px;
  }
`;

const Title = styled.div`
  font-size: 16px;
  margin-top: 4px;
  font-weight: 500;
  color: #fff;
`;

const Note = styled.div`
  font-size: 13px;
  margin-top: 4px;
  color: #fff;
`;

const Tot = styled.div`
  display: inline-block;
  height: 24px;
  line-height: 24px;
  vertical-align: top;
  margin-left: 4px;
  font-size: 13px;
  color: #fff;
  font-weight: 600;
`;
const Actions = styled.div`
  padding: 0 8px;
  margin-top: 8px;
  height: 40px;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  ${clearFix()};
`;
const Right = styled.div`
  float: right;
  height: 40px;
`;
const Members = styled.div`
  height: 40px;
  margin-top: 8px;
  font-size: 12px;
`;

const Img = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 50px;
  display: inline-block;
  margin-left: -4px;
  background-size: cover;
  border: 2px solid white;
`;

export default PlanComponent;
