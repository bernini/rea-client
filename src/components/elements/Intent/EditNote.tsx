import * as React from 'react';
import styled from '../../../themes/styled';
import Button, { LoaderButton } from '../../elements/Button/Button';
import Textarea from '../../inputs/TextArea/Textarea';
import { Mutation } from 'react-apollo';
const UPDATE_COMMITMENT = require('../../../graphql/mutations/updateCommitment.graphql');
import withNotif from '../../NotificationFactory';
import Commitment from '../../../types/Commitment';
import { withFormik, FormikProps, Field } from 'formik';
import * as Yup from 'yup';
import gql from 'graphql-tag';
import { compose } from 'recompose';

interface Props {
  handleNoteOpen(): boolean;
  intent: Commitment;
  onError: any;
  onSuccess: any;
}

interface FormValues {
  note: string;
}

interface MyFormProps {
  handleNoteOpen(): boolean;
  intent: Commitment;
}

const EditNoteComponent = (props: Props & FormikProps<FormValues>) => {
  const { handleNoteOpen, intent, values, onError, onSuccess } = props;
  return (
    <Mutation
      mutation={UPDATE_COMMITMENT}
      onError={onError}
      update={(store, { data: { updateCommitment } }) => {
        store.writeFragment({
          id: `${updateCommitment.commitment.__typename}-${
            updateCommitment.commitment.id
          }`,
          fragment: gql`
            fragment myCommitment on Commitment {
              id
              note
            }
          `,
          data: {
            __typename: 'Commitment',
            note: updateCommitment.commitment.note
          }
        });
        handleNoteOpen();
        return onSuccess();
      }}
    >
      {(editSentence, { data }) => (
        <Wrapper>
          <WrapperTextarea>
            <Field
              name={'note'}
              render={({ field }) => {
                return (
                  <Textarea
                    value={field.value}
                    name={field.name}
                    onChange={field.onChange}
                    placeholder={'Type a note...'}
                  />
                );
              }}
            />
          </WrapperTextarea>
          <EditButtons>
            <Button hovered onClick={handleNoteOpen}>
              Cancel
            </Button>
            <LoaderButton
              loading={props.isSubmitting}
              disabled={props.isSubmitting}
              text={'Edit Note'}
              onClick={() =>
                editSentence({
                  variables: {
                    token: localStorage.getItem('oce_token'),
                    id: intent.id,
                    note: values.note
                  }
                })
              }
            />
          </EditButtons>
        </Wrapper>
      )}
    </Mutation>
  );
};

const CompWithFormik = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => ({
    note: props.intent.note
  }),
  validationSchema: Yup.object().shape({
    note: Yup.string()
  }),
  handleSubmit: (values, { props }) => {
    console.log(values);
  }
})(EditNoteComponent);

export default compose(
  withNotif('Note updated correctly', 'Note is not updated')
)(CompWithFormik);

const Wrapper = styled.div``;

const WrapperTextarea = styled.div`
  height: 80px;
`;

const EditButtons = styled.div`
  margin-top: 8px;
  & button {
    margin-right: 8px;
  }
`;
