import { Mutation } from 'react-apollo';
import { Star } from '../Icons';
import React from 'react';
import styled from '../../../themes/styled';
// import getComm from "../../queries/getCommitment";
import { compose } from 'recompose';
const UPDATE_COMMITMENT = require('../../../graphql/mutations/updateCommitment.graphql');
import withNotif from '../../NotificationFactory';

interface Props {
  intentId: string;
  providerId?: string;
  onError: any;
  onSuccess: any;
}

const AddProviderComp: React.SFC<Props> = ({
  onError,
  onSuccess,
  intentId,
  providerId
}) => {
  return (
    <Mutation
      mutation={UPDATE_COMMITMENT}
      onError={onError}
      update={(store, { data: { updateCommitment } }) => {
        return onSuccess();
      }}
    >
      {(addProvider, { data }) => (
        <Span
          onClick={() =>
            addProvider({
              variables: {
                token: localStorage.getItem('oce_token'),
                id: intentId,
                providerId: providerId
              }
            })
          }
        >
          <Star width={18} height={18} strokeWidth={2} color={'#dadada'} />
        </Span>
      )}
    </Mutation>
  );
};

export const AddProviderComponent = compose(
  withNotif('Provider is successfully updated', 'Provider is not updated')
)(AddProviderComp);

const DeleteProviderComp: React.SFC<Props> = ({
  onError,
  onSuccess,
  intentId
}) => {
  return (
    <Mutation
      mutation={UPDATE_COMMITMENT}
      onError={onError}
      update={(store, { data: { updateCommitment } }) => {
        return onSuccess();
      }}
    >
      {(deleteProvider, { data }) => (
        <Span
          onClick={() =>
            deleteProvider({
              variables: {
                token: localStorage.getItem('oce_token'),
                id: intentId,
                providerId: 0
              }
            })
          }
        >
          <Star width={18} height={18} strokeWidth={1} color="#FFD054" />
        </Span>
      )}
    </Mutation>
  );
};

export const DeleteProvider = compose(
  withNotif('Provider is successfully updated', 'Provider is not updated')
)(DeleteProviderComp);

const Span = styled.span`
  display: inline-block;
  vertical-align: sub;
  cursor: pointer;
  &:hover {
    svg {
      color: #fdef24;
      stroke-width: 3px;
    }
  }
`;
