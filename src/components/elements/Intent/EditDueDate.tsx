import { Mutation } from "react-apollo";
import React from "react";
import styled, {css} from "../../../themes/styled";
// import getComm from "../../queries/getCommitment";
const UPDATE_COMMITMENT = require("../../../graphql/mutations/updateCommitment.graphql");
import moment from "moment";
import withNotif from "../../NotificationFactory";
// import gql from "graphql-tag";
import {compose} from 'recompose'
import { formatDate, parseDate } from "react-day-picker/moment";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
interface DueDateProps {
  value: any;
  action: any
}
const DueDate: React.SFC<DueDateProps> = props => {
  const FORMAT = "D/mmm/YYYY";

  return (
    <Wrapper>
      <DayPickerInput
        formatDate={formatDate}
        parseDate={parseDate}
        format={FORMAT}
        value={`${formatDate(props.value)}`}
        onDayChange={props.action}
      />
    </Wrapper>
  );
};

interface Props {
    intentId: string;
    due: any;
    onError: any
    onSuccess: any
}

const EditDueDate: React.SFC<Props> = ({ intentId, due, onError, onSuccess }) => {
  let duration = moment.duration(moment(due).diff(moment())).asHours();
    let date = new Date(due)
  return (
    <Mutation
      mutation={UPDATE_COMMITMENT}
      onError={onError}
      update={(store, { data: { updateCommitment } }) => {
        // const commitment = store.readFragment({
        //   id: `${updateCommitment.commitment.__typename}-${
        //     updateCommitment.commitment.id
        //   }`,
        //   fragment: gql`
        //     fragment myCommitment on Commitment {
        //       id
        //       due
        //     }
        //   `
        // });
        // commitment.due = updateCommitment.commitment.due;
        // store.writeQuery({
        //   query: getComm,
        //   data: commitment
        // });
        return onSuccess();
      }}
    >
      {(editDueDate, { data }) => (
        <DateWrapper deadline={duration < 0 ? "expired" : duration < 48 ? "soon" : ""}>
          <DueDate
            value={date}
            action={(value) => editDueDate({
              variables: {
                token: localStorage.getItem("oce_token"),
                id: intentId,
                due: moment(value).format("YYYY-MM-DD")
              }
            })}
          />
        </DateWrapper>
      )}
    </Mutation>
  );
};

export default compose(
  withNotif('Date updated correctly', 'Date is not updated correctly')
)(EditDueDate)

const Wrapper = styled.div`
cursor: pointer;
  & input {
    cursor: pointer;
    background: transparent;
    height: 24px;
    line-height: 24px;
    border: none;
    width: 60px;
    text-align: center;
    font-weight: 500;
    font-size: 12px;
  }
`
const DateWrapper = styled.div<{deadline?: string}>`
cursor: pointer;
  & input {
    color: #8d8d8d;
    width: 80px !important;
    font-size: 13px !important;
  }
  width: 80px;
  margin: 0;
  float: left;
  height: 24px;
  line-height: 24px;
  border-radius: 3px;
  margin-right: 8px;
  text-align: center;
  ${props =>
    props.deadline === "soon" &&
    css`
    & input {
      color: #ffffff;
    }
      background: #ffab00;
    `} ${props =>
    props.deadline === "expired" &&
    css`
    & input {
      color: #ffffff;
    }
      background: #ff5630;
    `};
`;
