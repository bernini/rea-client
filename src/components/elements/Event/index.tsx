import * as React from 'react';
import styled from '../../../themes/styled';
import { Check } from '../../elements/Icons';
import { clearFix } from 'polished';
import DeleteEvent from './DeleteEvent';

interface EventProps {
  image: string;
  id: string;
  primary: any;
  validations: any;
  openValidationModal?: any;
  secondary: string;
  date: string;
  withValidation?: boolean;
  withDelete?: boolean;
  providerId: string;
  light?: boolean;
  loggedUserId?: string;
  commitmentId: string;
  scopeId: string;
}

const Event: React.SFC<EventProps> = ({
  image,
  id,
  primary,
  validations,
  openValidationModal,
  secondary,
  date,
  light,
  withValidation,
  withDelete,
  commitmentId,
  scopeId,
  providerId,
  loggedUserId
}) => {
  return (
    <FeedItem>
      <Member>
        <MemberItem>
          <Img alt="provider" src={image} />
        </MemberItem>
      </Member>
      <Primary>
        {primary}
        <Date>{date}</Date>
        {validations.length > 0 ? (
          <Validations>
            validations
            {validations.map((a, i) => (
              <Check
                key={i}
                width={20}
                height={20}
                strokeWidth={2}
                color="green"
              />
            ))}
          </Validations>
        ) : null}
      </Primary>
      <Desc>
        {secondary ? <Secondary>{secondary}</Secondary> : null}
        <Sub>
          {withValidation || withDelete ? (
            <Actions>
              {withDelete && providerId === loggedUserId ? (
                <DeleteEvent
                  commitmentId={commitmentId}
                  eventId={id}
                  scopeId={scopeId}
                />
              ) : null}
              {/* {withValidation && providerId !== loggedUserId ? (
            <span>
              <Action onClick={() => openValidationModal(id)}>
                <Span>
                  <Star width={14} height={14} strokeWidth={1} color="#989ba0" />
                </Span>
                <ActionTitle>Validate</ActionTitle>
              </Action>
            </span>
          ) : null} */}
            </Actions>
          ) : null}
        </Sub>
      </Desc>
    </FeedItem>
  );
};

export default Event;

const FeedItem = styled.div`
  min-height: 30px;
  position: relative;
  margin: 0;
  padding: 8px;
  word-wrap: break-word;
  font-size: 14px;
  background: #f0f0f0;
  margin-bottom: 8px;
  border-radius: 4px;
  background: #fff;
  padding: 8px;
  border: 1px solid #e6ecf0;
  background: #fff;
  border: 1px solid #e4e4e4;
  border-radius: 3px;
  border: none;
  border-radius: 0;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  ${clearFix()};
  transition: background 0.5s ease;
`;

const Primary = styled.div`
  line-height: 20px;
  padding: 0;
  position: relative;
`;

const Secondary = styled.div`
  font-weight: 400;
  margin-top: 16px;
  letter-spacing. .5px;
  line-height: 20px;
  font-size: 14px;
  color: ${props => props.theme.styles.colour.base2}
`;

const Member = styled.div`
  float: left;
  vertical-align: top;
  margin-right: 8px;
`;
const Validations = styled.div`
  margin-top: 8px;
  vertical-align: sub;
  background: #ececec;
  display: inline-block;
  padding: 0 8px;
  border-radius: 3px;
  position: absolute;
  top: -10px;
  right: 10px;
  line-height: 20px;
  height: 30px;
  color: #3333338a;
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  letter-spacing: 0.5px;
  & svg {
    margin-top: 6px;
    vertical-align: bottom;
    margin-left: 4px;
  }
`;

const Sub = styled.div`
  ${clearFix()};
`;

const MemberItem = styled.span`
  background-color: #d6dadc;
  border-radius: 3px;
  color: #4d4d4d;
  display: inline-block;
  float: left;
  height: 40px;
  overflow: hidden;
  position: relative;
  width: 40px;
  user-select: none;
  z-index: 0;
`;

const Desc = styled.div`
  position: relative;
  min-height: 30px;
`;

const Img = styled.img`
  width: 40px;
  height: 40px;
  display: block;
  -webkit-appearance: none;
  line-height: 40px;
  text-indent: 4px;
  font-size: 13px;
  overflow: hidden;
  max-width: 40px;
  max-height: 40px;
  text-overflow: ellipsis;
`;

const Date = styled.div`
  font-size: 12px;
  letter-spacing: 1px;
  margin: 0;
  color: ${props => props.theme.styles.colour.base3};
`;

const Actions = styled.div`
  ${clearFix()};
  float: left;
  vertical-align: middle;
  margin-left: 0px;
`;

// const ActionTitle = styled.h3`
//   font-weight: 400;
//   margin-left: 8px;
//   display: inline-block;
//   height: 20px;
//   line-height: 32px;
//   font-size: 12px;
//   letter-spacing: 1px;
// `;

// const Action = styled.div`
//   cursor: pointer;
//   float: left;
//   transition: background-color 0.5s ease;
//   padding-right: 8px;
//   margin-right: 24px;
//   position: relative;
// `;

// const Span = styled.span`
//   vertical-align: sub;
//   margin-right: 0;
//   float: left;
//   height: 30px;
//   line-height: 30px;
//   & svg {
//     height: 30px;
//   }
// `;
