import { Mutation } from "react-apollo";
import {Trash} from '../Icons'
import * as React from "react";
import styled from '../../../themes/styled';
import { compose } from "recompose";
import gql from "graphql-tag";
import Commitment from '../../../types/Commitment'
import withNotif from "../../NotificationFactory";
const DELETE_EVENT = require("../../../graphql/mutations/deleteEvent.graphql");

export default compose(
  withNotif("Event successfully deleted", "error! evet had not been deleted")
)(({ eventId, commitmentId, onError, onSuccess }) => {
  return (
    <Mutation
      mutation={DELETE_EVENT}
      onError={e => onError()}
      update={(store, { data: { deleteEconomicEvent } }) => {
        const commitment: any = store.readFragment({
          id: `Commitment-${commitmentId}`,
          fragment: gql`
            fragment myCommitment on Commitment {
              fulfilledBy {
                fulfilledBy {
                  id
                  action
                }
              }
            }
          `
        });
        let eventIndexFromCommitment: Commitment = commitment.fulfilledBy.findIndex(
          ev => ev.fulfilledBy.id === eventId
        );
        commitment.fulfilledBy.splice(eventIndexFromCommitment, 1);
        store.writeFragment({
          id: `Commitment-${commitmentId}`,
          fragment: gql`
            fragment myCommitment on Commitment {
              fulfilledBy {
                fulfilledBy {
                  id
                  action
                }
              }
            }
          `,
          data: commitment
        });
        onSuccess()
      }}
    >
      {(deleteEvent, { data }) => (
        <Action
          onClick={() =>
            deleteEvent({
              variables: {
                token: localStorage.getItem("oce_token"),
                id: eventId
              }
            })
          }
        >
          <Span>
            <Trash width={13} height={13} strokeWidth={1} color="#989ba0" />
          </Span>
          <ActionTitle>Delete</ActionTitle>
        </Action>
      )}
    </Mutation>
  );
});

const ActionTitle = styled.h3`
  font-weight: 400;
  display: inline-block;
  height: 20px;
  line-height: 32px;
  font-size: 12px;
  letter-spacing: 1px;
  margin: 0;
  margin-left: 8px;
  color: ${props => props.theme.styles.colour.base3};
`;

const Action = styled.div`
  cursor: pointer;
  float: left;
  position: relative;
  padding-right: 8px;
  margin-right: 24px;
  transition: background-color 0.5s ease;
  &:after {
    position: absolute;
    content: "";
    width: 2px;
    height: 2px;
    background: ${props => props.theme.styles.colour.base3};
    display: block;
    right: -8px;
    top: 15px;
    border-radius: 100px;
  }
`;

const Span = styled.span`
  vertical-align: sub;
  margin-right: 0
  float: left;
  height: 30px;
  line-height: 30px;
  & svg {
    height: 30px;
  }
`;
