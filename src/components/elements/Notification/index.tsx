import React from "react";
import styled, { css } from "styled-components";
import { clearFix, animation } from "polished";
import {Cross} from "../Icons";

const Wrapper = styled.div<{type?: any}>`
  background-color: #0052CC;
  border-radius: 4px;
  padding: 15px;
  margin-bottom: 10px;
  ${animation("slidein", "1s", "ease-in-out")}
  ${clearFix()};
  ${props =>
    props.type === "alert" &&
    css`
      background: #ff5630;
      color: white;
    `}
  ${props =>
    props.type === "warning" &&
    css`
      background: #ffab00;
      color: #091e42;
    `}
  ${props =>
    props.type === "success" &&
    css`
      background: #36b37e;
      color: #091e42;
    `}
`;

const Content = styled.div<{type?: any}>`
  width: 90%;
  display: inline-block;
  color: white;
  ${props =>
    props.type === "alert" &&
    css`
      color: white;
    `}
  ${props =>
    props.type === "warning" &&
    css`
      color: #091e42;
    `}
  ${props =>
    props.type === "success" &&
    css`
      color: #091e42;
    `}
`;

const Actions = styled.div`
  float: right;
  cursor: pointer;
  height: 18px;
`;

const Container = styled.div``;

interface Notification {
  type: string
  id: string
  message: string
}

interface Props {
    notifications: Notification[]
    dismiss: any
}

const Notification: React.SFC<Props> = ({ notifications, dismiss }) => {
  console.log(notifications)
  console.log(dismiss)
  return (
    <Container>
      {notifications.map((el, i) => (
        <Wrapper key={i} type={el.type}>
          <Content type={el.type}>{el.message}</Content>
          <Actions
            onClick={() =>
              dismiss({
                variables: {
                  id: el.id
                }
              })
            }
          >
            <Cross width={18} strokeWidth={1} height={18}  color={"#fff"} />
          </Actions>
        </Wrapper> 
      ))}
    </Container>
  );
};

export default Notification;
