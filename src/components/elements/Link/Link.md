```js
const { BrowserRouter } = require('react-router-dom');

<BrowserRouter>
  <div>
    <Link to="#">Default</Link>
    <br />
    <Link to="#" hovered>
      Hovered
    </Link>
    <br />
    <Link to="#" active>
      Pressed
    </Link>
  </div>
</BrowserRouter>;
```
