import * as React from 'react';
import styled from '../../../themes/styled';
import { ellipsis } from 'polished';

interface Props {
  toggleInfo: any;
  toggleSkills: any;
  logout: any;
}

const MenuHeader: React.SFC<Props> = ({ logout }) => {
  return (
    <Menu>
      <List>
        {/* <Item onClick={toggleInfo}>Edit profile</Item>
        <Item onClick={toggleSkills}>Edit skills</Item> */}
        <Item onClick={logout}>Logout</Item>
      </List>
    </Menu>
  );
};

const Menu = styled.div`
  background: #fff;
  border-radius: 4px;
  box-shadow: 0 10px 15px 0 rgba(0, 0, 0, 0.06);
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  position: absolute;
  top: 40px;
  right: 0px;
  border: 1px solid #d5dce0;
  width: 220px;
`;
const List = styled.div`
  padding: 5px 0;
  border-bottom: 1px solid #e0e6e8;
  & a {
    display: block;
    text-decoration: none;
  }
  &:last-of-type {
    border-bottom: 0px;
  }
`;
const Item = styled.div`
  color: ${props => props.theme.styles.colour.base1};
  padding: 0 10px;
  line-height: 36px;
  font-size: 14px;
  ${ellipsis('220px')};
  display: block;
  text-decoration: none;
  &:hover {
    background: #d6eaff;
  }
`;

export default MenuHeader;
