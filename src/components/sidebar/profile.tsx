import * as React from "react";
import styled from "../../themes/styled";
import { clearFix } from "polished";
import * as Individual from "../../icons/paw_print.png";
import * as Group from "../../icons/paws.png";

interface Props {
  providerName: string;
  type: string
}

const ProfileHeader: React.SFC<Props> = ({
  providerName,
  type,
}) => (
  <Profile>
    <Header>
      <Title>{providerName}</Title>
    </Header>
      <Type>
        {type === "Person" ? (
          <SpanIcon style={{ backgroundImage: `url(${Individual})` }} />
        ) : (
          <SpanIcon style={{ backgroundImage: `url(${Group})` }} />
        )}
        {type === "Person" ? 'Human' : type}
      </Type>
  </Profile>
);

const SpanIcon = styled.div`
  cursor: pointer;
  margin-right: 8px;
  display: inline-block;
  width: 18px;
  height: 18px;
  background-size: contain;
  vertical-align: sub;
`;

const Header = styled.div`
  ${clearFix()};
`;
const Type = styled.div`
  ${clearFix()};
  color: ${props => props.theme.styles.colour.base3};
  margin-top: 4px;
  margin-bottom: 10px;
  font-size: 12px;
  font-weight: 400;
  text-transform: uppercase;
`;

const Profile = styled.div`
  background: white;
  border: 1px solid #e4e4e4;
  border-radius: 4px;
  padding-left: 10px;
  box-shadow: 0px 1px 2px 0px rgba(0,0,0,.1);
`;

const Title = styled.h3`
  line-height: 26px;
  color: ${props => props.theme.styles.colour.base1};
  margin-left: 0;
  font-size: 16px;
  font-weight: 500;
  margin: 0;
  margin-top: 8px;
`;

export default ProfileHeader;
