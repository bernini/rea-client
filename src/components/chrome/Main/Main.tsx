import styled from '../../../themes/styled';

export default styled.div`
  padding: 10px 15px;
  overflow: auto;
  display: flex;
  flex: 1;
`;
