import { compose } from 'recompose';
import moment from 'moment';
import { withFormik, FormikProps, Form, Field } from 'formik';
import * as Yup from 'yup';
import React from 'react';
import Button from '../elements/Button/Button';
import Input from '../inputs/Text/Text';
import { ApolloConsumer } from 'react-apollo';
import styled from 'styled-components';
import AsyncSelect from 'react-select/lib/Async';
// import Alert from "../alert";
import { clearFix, placeholder } from 'polished';
import { getAllResources } from '../../helpers/asyncQueries';
import Select from 'react-select';
import Events from '../../util/events';
import { formatDate, parseDate } from 'react-day-picker/moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
// import withNotif from "../notification";
import { graphql, OperationOption } from 'react-apollo';
const CREATE_COMMITMENT = require('../../graphql/mutations/createCommitment.graphql');
const getCommitments = require('../../graphql/queries/getCommitments.graphql');

interface Props {
  action: any;
  note: string;
  unit: any;
  due: any;
  affectedResourceClassifiedAsId: any;
  numericValue: string;
  scopeId: string;
  closeSmartSentence?: any;
}

interface MyFormProps {
  providerId: string;
  scopeId: string;
  createCommitmentMutation: any;
  toggleModal: any;
  note: string;
  action: any;
  unit: any;
  due: any;
  affectedResourceClassifiedAsId: any;
  numericValue: string;
  closeSmartSentence?: any;
}

interface Values {
  value: string;
  label: string;
}

interface FormValues {
  action: any;
  unit: any;
  due: any;
  affectedResourceClassifiedAsId: any;
  numericValue: string;
}

const LogEvent = (props: Props & FormikProps<FormValues>) => {
  const FORMAT = 'D/mmm/YYYY';
  return (
    <ApolloConsumer>
      {client => (
        <Form>
          <Module>
            <Row>
              <Action>
                <Field
                  name={'action'}
                  render={({ field }) => {
                    return (
                      <Select
                        onChange={(val: Values) =>
                          props.setFieldValue('action', {
                            value: val.value,
                            label: val.label
                          })
                        }
                        options={Events}
                        // styles={customStyles}
                        value={field.value}
                        placeholder="Select an event"
                      />
                    );
                  }}
                />
                {props.errors.action &&
                  props.touched.action &&
                  props.errors.action}
              </Action>
              <Qty>
                <Value>
                  <Field
                    name="numericValue"
                    render={({ field }) => (
                      <Input
                        name={field.name}
                        onChange={field.onChange}
                        type="number"
                        min="00.00"
                        max="100.00"
                        step="0.1"
                        placeholder="00.00"
                      />
                    )}
                  />
                </Value>
              </Qty>
              {props.errors.numericValue &&
                props.touched.numericValue &&
                props.errors.numericValue}
              <Resource>
                <Field
                  name="affectedResourceClassifiedAsId"
                  render={({ field }) => (
                    <AsyncSelect
                      placeholder="Select a classification..."
                      defaultOptions
                      cacheOptions
                      value={field.value}
                      onChange={val => {
                        props.setFieldValue('unit', {
                          value: val.value.unitId,
                          label: val.value.unitName
                        });
                        return props.setFieldValue(
                          'affectedResourceClassifiedAsId',
                          {
                            value: val.value.value,
                            label: val.label
                          }
                        );
                      }}
                      loadOptions={val =>
                        getAllResources(client, props.scopeId, val)
                      }
                    />
                  )}
                />
                {props.errors.affectedResourceClassifiedAsId &&
                  props.touched.affectedResourceClassifiedAsId &&
                  props.errors.affectedResourceClassifiedAsId}
              </Resource>
            </Row>
            <WrapperDate>
              <Field
                name="due"
                render={({ field }) => (
                  <DayPickerInput
                    formatDate={formatDate}
                    parseDate={parseDate}
                    format={FORMAT}
                    value={`${formatDate(field.value)}`}
                    onDayChange={val => {
                      props.setFieldValue('due', val);
                    }}
                  />
                )}
              />
            </WrapperDate>

            <PublishActions>
              <Button type="submit">Publish</Button>
              <Button hovered onClick={props.closeSmartSentence}>
                Cancel
              </Button>
            </PublishActions>
          </Module>
        </Form>
      )}
    </ApolloConsumer>
  );
};

const withCreateCommitment = graphql<{}>(CREATE_COMMITMENT, {
  name: 'createCommitmentMutation'
  // TODO enforce proper types for OperationOption
} as OperationOption<{}, {}>);

const CompWithFormik = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => ({
    action: '',
    numericValue: '00.00' || '',
    unit: null,
    due: new Date(),
    affectedResourceClassifiedAsId: null
  }),
  validationSchema: Yup.object().shape({
    action: Yup.string().required(),
    numericValue: Yup.number(),
    unit: Yup.object(),
    due: Yup.string().required(),
    affectedResourceClassifiedAsId: Yup.object().required()
  }),
  handleSubmit: (values, { props, resetForm, setErrors, setSubmitting }) => {
    let date = moment(values.due).format('YYYY-MM-DD');
    setSubmitting(true);
    let MutationVariables = {
      token: localStorage.getItem('oce_token'),
      action: values.action.value.toLowerCase(),
      due: date,
      note: props.note,
      committedResourceClassifiedAsId:
        values.affectedResourceClassifiedAsId.value,
      committedUnitId: values.unit.value,
      committedNumericValue: values.numericValue,
      scopeId: props.scopeId
    };
    return props
      .createCommitmentMutation({
        variables: MutationVariables,
        update: (store, { data }) => {
          let comm = store.readQuery({
            query: getCommitments,
            variables: {
              token: localStorage.getItem('oce_token'),
              id: props.scopeId
            }
          });
          comm.viewer.agent.agentCommitments.push(
            data.createCommitment.commitment
          );
          store.writeQuery({
            query: getCommitments,
            data: comm,
            variables: {
              token: localStorage.getItem('oce_token'),
              id: props.scopeId
            }
          });
        }
      })
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }
})(LogEvent);

export default compose(withCreateCommitment)(CompWithFormik);

const Module = styled.div`
  ${clearFix()};
`;
const WrapperDate = styled.div`
  ${clearFix()};
  margin-left: 8px;
  & input {
    height: 40px;
    border-radius: 4px;
    border: 0;
    border: 1px solid #c9c9c9;
    font-size: 16px;
    text-align: center;
    width: 120px;
  }
`;
const Action = styled.div``;

const PublishActions = styled.div`
  background: #ecedee;
  height: 45px;
  padding: 0 10px;
  margin-top: 10px;
  ${clearFix()};
  & button {
    width: 120px;
    margin: 0;
    float: right;
    box-shadow: none;
    margin-top: 6px;
    height: 34px;
    line-height: 34px;
    font-size: 14px;
    letter-spacing: 0.5px;
    letter-spacing: 0.5px;
    margin-top: 6px;
    border-radius: 2px;
    border: 1px solid #2a668f;
    margin-left: 10px;
  }
`;

const Value = styled.div``;

const Qty = styled.div`
  max-height: 36px;
  text-align: center;
  display: flex;
  box-sizing: border-box;
  background: white;
  padding: 0 4px;
  & div {
    width: 100%;
  }
  & input {
    width: 100%;
    float: left;
    text-align: center;
    color: #333;
    height: 34px;
    border-color: #cccccc !important;
    background: transparent;
    font-size: 16px;
    height: 40px;
    max-height: 38px !important;
    min-height: 30px;
    border-width: 1px !important;
    ${placeholder({ color: 'hsl(0,0%,50%) !important', fontSize: '16px' })};
  }
`;

const Resource = styled.div`
  margin-bottom: 8px;
  z-index: 999;
  position: relative;
`;
const Row = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr 3fr;
  grid-column-gap: 4px;
  padding: 0 8px;
  & input {
    ${placeholder({ color: '#333' })};
  }
`;
