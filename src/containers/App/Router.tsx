import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import styled from '../../themes/styled';
import Login from '../../pages/login/Login';
import NotFound from '../../pages/not-found/NotFound';
import ProtectedRoute from './ProtectedRoute';
import Home from '../../pages/home';
import Agent from '../../pages/agent';
// import Community from '../../pages/communities.community/CommunitiesCommunity'
import Plan from '../../pages/Plan/Plan';
import Search from '../../pages/search';
import Header from '../../components/elements/Header/Header';
// import Sidebar from '../../components/sidebar/sidebar';
// import Feed from '../../components/elements/Feed/Feed'
// import Search from '../../pages/search/Search';
const getNotification = require('../../graphql/queries/getNotifications');
const deleteNotification = require('../../graphql/mutations/DeleteNotification');
import { compose } from 'recompose';
import { Query, graphql } from 'react-apollo';
import Notifications from '../../components/elements/Notification';
import CommunitiesAll from '../../pages/communities.all/CommunitiesAll';

const GenericAlert = styled.div`
  position: fixed;
  top: 10px;
  width: 320px;
  right: 10px;
  z-index: 99999999999999999999999999999999;
`;

/**
 * The application routes definition.
 *
 * Note that all routes requiring the user to be authenticated are within
 * the ProtectedRoute component, which then delegates further routing to a
 * Switch route component.
 */
const AppInner = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 100%;
`;

const NotificationsTemplate = props => {
  return (
    <GenericAlert>
      <Notifications
        notifications={props.notifications}
        dismiss={props.deleteNotification}
      />
    </GenericAlert>
  );
};

const EhnanchedNotifications = compose(
  graphql(deleteNotification, { name: 'deleteNotification' })
)(NotificationsTemplate);

export default () => (
  <Router>
    <AppInner>
      <Query query={getNotification}>
        {({ data: { notifications } }) => {
          console.log('notifications');
          return <EhnanchedNotifications notifications={notifications} />;
        }}
      </Query>
      <Switch>
        <Route exact path="/login" component={Login} />
        <ProtectedRoute
          path="/"
          component={props => {
            return (
              <Container>
                <Whole>
                  {/* <Sidebar
                    param={props.match.params.id}
                    location={props.location}
                  /> */}
                  <Surface>
                    <Header history={props.history} />
                    <Switch>
                      <Route path="/communities/:id" component={Agent} />
                      <Route path="/plan/:id" component={Plan} />
                      <Route path="/search/:id" component={Search} />
                      <Route exact path="/communities" component={CommunitiesAll} />
                      <Route path="/" component={Home} />
                      <Route component={NotFound} />
                    </Switch>
                  </Surface>
                  {/* <Feed /> */}
                </Whole>
              </Container>
            );
          }}
        />
        {/* <Route component={NotFound} /> */}
      </Switch>
    </AppInner>
  </Router>
);

const Surface = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  flex: 1;
`;

const Container = styled.div`
  display: flex;
  margin: 0;
  height: 100vh;
  overflow-y: hidden;
  flex: 1;
`;

const Whole = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 100%;
`;
